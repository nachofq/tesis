# =============================================================================
# Imports and definitions
# =============================================================================
import numpy as np
import matplotlib.pyplot as plt
import scipy.io
from calcularHyhxSCADA_PMU_Weighted import *
from calcularHyhxOnlySCADA import *

from pypower.api import \
    runpf as runpf, \
    case14 as case14, \
    case30 as case30, \
    ppoption as ppoption, \
    case4gs as case4, \
    case6ww as case6
from math import pi as pi, sin as sin, cos as cos, e as e

def case13():
     case = {}
     
     case['version'] = '2'
     case['baseMVA'] = 100
     case['bus'] = np.array([1,3,0,0,0,0,1,1.03,18.5,345,1,1.1,0.9,2,2,0,0,0,0,1,1.01,8.80,345,1,1.1,0.9,3,1,0,0,0,3,1,0.9781,-6.1,345,1,1.5,0.5,4,1,9.76,1,0,0,1,0.95,-10,345,1,1.05,0.95,5,1,0,0,0,0,1,1.0103,12.1,345,1,1.5,0.5,6,2,0,0,0,0,1,1.03,-6.8,345,1,1.1,0.9,7,2,0,0,0,0,1,1.01,-16.9,345,1,1.1,0.9,8,1,0,0,0,5,1,0.9899,-31.8,345,1,1.5,0.5,9,1,17.65,1,0,0,1,0.95,-35,345,1,1.05,0.95,10,1,0,0,0,0,1,0.9876,2.1,345,1,1.5,0.5,11,2,0,0,0,0,1,1,-19.3,345,1,1.5,0.5,12,1,0,0,0,0,1,1.0125,-13.4,345,1,1.5,0.5,13,1,0,0,0,0,1,0.9938,-23.6,345,1,1.5,0.5]).reshape(13,13)
     case['gen'] = np.array([1,7.00,1.61,5.0,-1,1.03,100,1,25,0.01,0,0,0,0,0,0,0,0,0,0,0,2,7.00,1.76,5.0,-1,1.01,100,1,30,0.01,0,0,0,0,0,0,0,0,0,0,0,6,7.16,1.49,5.0,-1,1.03,100,1,27,0.01,0,0,0,0,0,0,0,0,0,0,0,7,7.00,1.39,5.0,-1,1.01,100,1,27,0.01,0,0,0,0,0,0,0,0,0,0,0,11,0.00,1.09,2,0,1,100,1,27,0.01,0,0,0,0,0,0,0,0,0,0,0]).reshape(5,21)
     case['branch'] = np.array([1,5,0,0.0167,0,250,250,250,0,0,1,-360,360,2,10,0.0,0.0167,0.00,250,250,250,0,0,1,-360,360,3,4,0.0,0.005,0.00,150,150,150,0,0,1,-360,360,3,10,0.001,0.0100,0.0175,300,300,300,0,0,1,-360,360,3,11,0.011,0.110,0.1925,150,150,150,0,0,1,-360,360,3,11,0.011,0.110,0.1925,250,250,250,0,0,1,-360,360,5,10,0.0025,0.025,0.0437,250,250,250,0,0,1,-360,360,6,12,0.0,0.0167,0.0,250,250,250,0,0,1,-360,360,7,13,0.0,0.0167,0.0,250,250,250,0,0,1,-360,360,8,11,0.011,0.11,0.1925,250,250,250,0,0,1,-360,360,8,11,0.011,0.11,0.1925,250,250,250,0,0,1,-360,360,8,9,0.0,0.005,0.00,250,250,250,0,0,1,-360,360,8,13,0.001,0.01,0.0175,250,250,250,0,0,1,-360,360,12,13,0.0025,0.025,0.0437,250,250,250,0,0,1,-360,360]).reshape(14,13)
    
     return case

def cc():
    plt.close('all')

class Yreord:  
    def __init__(self,nodosObs,nodosNoObs,Y):  
        self.YOO = np.empty((len(nodosObs),len(nodosObs)),dtype=np.complex_)
        self.YOU = np.empty((len(nodosObs),len(nodosNoObs)),dtype=np.complex_)
        self.YUO = np.empty((len(nodosNoObs),len(nodosObs)),dtype=np.complex_)
        self.YUU = np.empty((len(nodosNoObs),len(nodosNoObs)),dtype=np.complex_)
        
        ii = 0
        jj = 0
        kk = 0
        for i in nodosObs:
            for j in nodosObs:
                self.YOO[(ii,jj)] = Y[(i-1,j-1)]
                jj = jj + 1
            for k in nodosNoObs:
                self.YOU[(ii,kk)] = Y[(i-1,k-1)]
                kk = kk +1
            jj = 0
            kk = 0
            ii = ii +1
        
        ii = 0
        jj = 0
        kk = 0
        for i in nodosNoObs:
            for j in nodosObs:
                self.YUO[(ii,jj)] = Y[(i-1,j-1)]
                jj = jj + 1
            for k in nodosNoObs:
                self.YUU[(ii,kk)] = Y[(i-1,k-1)]
                kk = kk +1
            jj = 0
            kk = 0
            ii = ii +1
            
        Yaux1 = np.append(self.YOO,self.YOU,axis=1)    
        Yaux2 = np.append(self.YUO,self.YUU,axis=1)
        self.Y = np.append(Yaux1,Yaux2,axis=0)   

def ordenarVectorEstados(EO, EU, nodosO, nodosU):
    estadoActual = {}
    estadoActual['Phase']               = np.zeros((len(EO)+len(EU)))
    estadoActual['Mag']                 = np.zeros((len(EO)+len(EU)))
    estadoActual['V']                 = np.zeros((len(EO)+len(EU)),dtype=complex)
    jj = 0
    for k in nodosO:
        estadoActual['Phase'][k-1]      = np.angle(EO[jj])
        estadoActual['Mag'][k-1]        = np.absolute(EO[jj])
        estadoActual['V'][k-1]          = EO[jj]
        jj = jj+1
    jj=0
    for k in nodosU:
        estadoActual['Phase'][k-1]      = np.angle(EU[jj])
        estadoActual['Mag'][k-1]        = np.absolute(EU[jj])
        estadoActual['V'][k-1]          = EU[jj]
        jj = jj+1   
    return estadoActual


  


def variacionEstadoAnteriorActual(estadoAnterior,estadoActual,rE):
    estimVActual = estadoAnterior['V']
    estimVAnterior = estadoActual['V']
    
    variacionVActualVsAnterior  = np.abs(estimVActual - estimVAnterior)\
    *100/np.abs(estimVAnterior)            
      
    SestimAntF = np.zeros((rE.cantRamas),dtype=np.complex_)                 
    SestimAntT = np.zeros((rE.cantRamas),dtype=np.complex_)                 
    SestimActF = np.zeros((rE.cantRamas),dtype=np.complex_)                 
    SestimActT = np.zeros((rE.cantRamas),dtype=np.complex_)                     
    jjj = 0
    for rama in rE.RamaLst:           
        nodoF = rama['from'] 
        nodoT = rama['to']                              
        aux_SestimAntF = np.conjugate((rama['Ybr'][0][0]*estimVAnterior[nodoF-1]\
                                       + rama['Ybr'][0][1]*estimVAnterior[nodoT-1]))\
                                       *estimVAnterior[nodoF-1]
        aux_SestimAntT = np.conjugate((rama['Ybr'][1][0]*estimVAnterior[nodoF-1]\
                                       + rama['Ybr'][1][1]*estimVAnterior[nodoT-1]))\
                                       *estimVAnterior[nodoT-1]
        aux_SestimActF = np.conjugate((rama['Ybr'][0][0]*estimVActual[nodoF-1]\
                                       + rama['Ybr'][0][1]*estimVActual[nodoT-1]))\
                                       *estimVActual[nodoF-1]
        aux_SestimActT = np.conjugate((rama['Ybr'][1][0]*estimVActual[nodoF-1]\
                                       + rama['Ybr'][1][1]*estimVActual[nodoT-1]))\
                                       *estimVActual[nodoT-1]                      
        SestimAntF[jjj] = aux_SestimAntF            
        SestimAntT[jjj] = aux_SestimAntT
        SestimActF[jjj] = aux_SestimActF
        SestimActT[jjj] = aux_SestimActT
        jjj = jjj +1
        
    # CALCULO DE VARIACIONES
    variacionSFActualVsAnterior = np.abs(SestimAntF-SestimActF)*100/np.abs(SestimAntF)
    variacionSTActualVsAnterior = np.abs(SestimAntT-SestimActT)*100/np.abs(SestimAntT)

    return variacionVActualVsAnterior, variacionSFActualVsAnterior, variacionSTActualVsAnterior

def zonasAfectadas(obs,varSF,umbralZona1,umbralZona2,rE):
    
    nodosO                          = obs['nodosO']
    nodosU                          = obs['nodosU']
    ramasOU                         = []
    ramasUO                         = []
    ramasUU                         = []
    indexRamasOU                    = []
    indexRamasUO                    = []
    indexRamasUU                    = []
    
    ramasOUZona1                    = []
    ramasOUZona2                    = []
    
    ramasUOZona1                    = []
    ramasUOZona2                    = []
    
    ramasUUZona1                    = []
    ramasUUZona2                    = []

    nodosUZona1                     = []
    nodosUZona2                     = []
    nodosUZona3                     = []
    
    maxVarSF                        = np.max(varSF)
    
    
    for rama in rE.RamaLst:
        if (rama['from'] in nodosO)and(rama['to'] in nodosU):
            ramasOU = ramasOU + [np.append(rama['from'],rama['to'])]   
            indexRamasOU = indexRamasOU + [rama['ramaN']]
        if (rama['from'] in nodosU)and(rama['to'] in nodosO):
            ramasUO = ramasUO + [np.append(rama['from'],rama['to'])]   
            indexRamasUO = indexRamasUO + [rama['ramaN']]
        if (rama['from'] in nodosU)and(rama['to'] in nodosU):
            ramasUU = ramasUU + [np.append(rama['from'],rama['to'])]   
            indexRamasUU = indexRamasUU + [rama['ramaN']]    
     
        variacionSFRamasOU   = [varSF[k-1] for k in indexRamasOU]
        variacionSFRamasUO   = [varSF[k-1] for k in indexRamasUO]
        variacionSFRamasUU   = [varSF[k-1] for k in indexRamasUU]
    
    print('-'*85)    
    print("Variaciones detectadas en ramas OU")
    for i in range(len(ramasOU)):              
        varRelativaAlMax = variacionSFRamasOU[i]*100/maxVarSF
        if varRelativaAlMax > umbralZona1:
            print("Rama:",ramasOU[i],"\t| ","Variacion detectada relativa al maximo [%]",round(varRelativaAlMax,4), "\t | Zona 1") 
            ramasOUZona1 = ramasOUZona1 + [indexRamasOU[i],[ramasOU[i]]]
            nodosUZona1  = nodosUZona1 + [ramasOU[i][1]]
        elif varRelativaAlMax > umbralZona2 and  varRelativaAlMax < umbralZona1:
            print("Rama:",ramasOU[i],"\t| ","Variacion detectada relativa al maximo [%]",round(varRelativaAlMax,4), "\t | Zona 2") 
            ramasOUZona2 = ramasOUZona2 + [indexRamasOU[i],[ramasOU[i]]]
            nodosUZona2  = nodosUZona2 + [ramasOU[i][1]]
        else:
            print("Rama:",ramasOU[i],"\t| ","Variacion detectada relativa al maximo [%]",round(varRelativaAlMax,4), "\t | OK") 
    print('-'*60)
    print("Variaciones detectadas en ramas UO")
    for i in range(len(ramasUO)):
        varRelativaAlMax = variacionSFRamasUO[i]*100/maxVarSF
        if varRelativaAlMax > umbralZona1:
            print("Rama:",ramasUO[i],"\t| ","Variacion detectada relativa al maximo [%]",round(varRelativaAlMax,4), "\t | Zona 1") 
            ramasUOZona1 = ramasUOZona1 + [indexRamasUO[i],[ramasUO[i]]]
            nodosUZona1  = nodosUZona1 + [ramasUO[i][0]]
        elif varRelativaAlMax > umbralZona2 and  varRelativaAlMax < umbralZona1:
            print("Rama:",ramasUO[i],"\t| ","Variacion detectada relativa al maximo [%]",round(varRelativaAlMax,4), "\t | Zona 2") 
            ramasUOZona2 = ramasUOZona2 + [indexRamasUO[i],[ramasUO[i]]]
            nodosUZona2  = nodosUZona2 + [ramasUO[i][0]]
        else:
            print("Rama:",ramasUO[i],"\t| ","Variacion detectada relativa al maximo [%]",round(varRelativaAlMax,4), "\t | OK") 
    print('-'*60)
    print("Variaciones detectadas en ramas UU")
    for i in range(len(ramasUU)):
        varRelativaAlMax = variacionSFRamasUU[i]*100/maxVarSF
        if varRelativaAlMax > umbralZona1:
            print("Rama:",ramasUU[i],"\t| ","Variacion detectada relativa al maximo [%]",round(varRelativaAlMax,4), "\t | Zona 1") 
            ramasUUZona1 = ramasUUZona1 + [indexRamasUU[i],[ramasUU[i]]]
            nodosUZona1  = nodosUZona1 + [ramasUU[i][0]] + [ramasUU[i][1]]
        elif varRelativaAlMax > umbralZona2 and  varRelativaAlMax < umbralZona1:
            print("Rama:",ramasUU[i],"\t| ","Variacion detectada relativa al maximo [%]",round(varRelativaAlMax,4), "\t | Zona 2") 
            ramasUUZona2 = ramasUUZona2 + [indexRamasUU[i],[ramasUU[i]]]
            nodosUZona2  = nodosUZona2 + [ramasUU[i][0]] + [ramasUU[i][1]]                    
        else:
            print("Rama:",ramasUU[i],"\t| ","Variacion detectada relativa al maximo [%]",round(varRelativaAlMax,4), "\t | OK") 
    print('-'*60)

    


    nodosUZona1 = nodosUZona1 + [a for a in nodosUZona2 if a in nodosUZona1]
    nodosUZona1 = np.unique(nodosUZona1).tolist()
    print('Zona1: ',nodosUZona1 )
    nodosUZona2 = [a for a in nodosUZona2 if a not in nodosUZona1] 
    nodosUZona2 = np.unique(nodosUZona2).tolist()
    print('Zona2: ',nodosUZona2 )   
    
    print('='*85)
    print('='*85)
    print('='*85,'\n\n') 
    return nodosUZona1,nodosUZona2

def calculoMatA(obs,Pi,Qi,Vi,Ynew):
    aux = np.empty(len(obs['nodosU']),dtype=np.complex_)
    ii = 0
    for i in obs['nodosU']:
        aux[ii] = (Pi[i-1]-1j*Qi[i-1])/(Vi[i-1]**2) 
        ii = ii + 1
    YU = np.diag(aux)
    YT = Ynew.YUU - YU 
    A  = np.matmul(-np.linalg.inv(YT),Ynew.YUO)
    return(A,YT,YU)    

def calcYUfromEstados(nodosNoObs,estado,rE):  
    Iconj = np.conj(np.matmul(rE.Y,estado['V']))
    S = estado['V']*Iconj
    aux = np.empty(len(nodosNoObs),dtype=np.complex_)
    ii = 0
    for i in nodosNoObs:      
        Pi = S[i-1].real
        Qi = S[i-1].imag
        Vi = np.abs(estado['V'])[i-1]
        aux[ii] = (Pi-1j*Qi)/(Vi**2) 
        ii = ii + 1
    YU = np.diag(aux)
    return YU

def correccionA(A,refFaseNodo,YT,YU,estado_actual,medicionActual,obs,zona1,zona2,rE,signal):
    
    iteracionWLS=0 
    totalIteracionesWLS = 150
    pasoWLS = 0.1
    cotaTVE = 0.005
    auxEstadoActual = {}   
    
    V = {}
    Videal = {}
    Videal['Phase'] = np.angle(signal)
    Videal['Mag'] = np.abs(signal)
    Videal['V']   = np.array(Videal['Mag'])*(np.cos(Videal['Phase'])+1j*(np.sin(Videal['Phase'])))

    V['Phase'] = estado_actual['Phase']
    V['Mag'] = estado_actual['Mag']
    V['V']   = estado_actual['V']
    
    
    xk = [a for a in V['Phase']][1:]
    xk = xk + [a for a in V['Mag']]     
    [H,hx,W]=calcularHyhxSCADA_PMU_Weighted2(V,obs['nodosO'],zona1,zona2,rE) 
    
    auxEstadoActual['Phase'] = np.array([refFaseNodo] + xk[:rE.cantNodos-1])
    auxEstadoActual['Mag'] = np.array(xk[rE.cantNodos-1:])
    auxEstadoActual['V']   = np.array(auxEstadoActual['Mag'])*(np.cos(auxEstadoActual['Phase'])+1j*(np.sin(auxEstadoActual['Phase'])))

    estados={}                                  # PLOT DE PRUEBA
    estados['Mag']   = auxEstadoActual['Mag']   # PLOT DE PRUEBA
    estados['Phase'] = auxEstadoActual['Phase'] # PLOT DE PRUEBA
   
    #While ARMAR EL LOOP POR AHORA ITERA SOLO UNA VEZ.
    while(iteracionWLS < totalIteracionesWLS):
        H = H[:,1:]
        HT = np.transpose(H)
        Dif_Med_hx = medicionActual - hx                
        G = np.matmul(W,H).real
        G = np.matmul(HT,G).real       
        g = np.matmul(-HT,W).real        
        g = (g.dot(Dif_Med_hx)).real 
               
        xkp1 = (xk - pasoWLS*np.linalg.inv(G).dot(g)   ).tolist()
  
        auxEstadoActual['Phase'] = np.array([refFaseNodo] + xkp1[:rE.cantNodos-1])
        auxEstadoActual['Mag'] = np.array(xkp1[rE.cantNodos-1:])
        auxEstadoActual['V']   = np.array(auxEstadoActual['Mag'])*(np.cos(auxEstadoActual['Phase'])+1j*(np.sin(auxEstadoActual['Phase'])))
     
          
        [H,hx,W]=calcularHyhxSCADA_PMU_Weighted2(auxEstadoActual,obs['nodosO'],zona1,zona2,rE)                   
        xk = xkp1
        iteracionWLS = iteracionWLS +1 
        estados['Mag'] = np.column_stack([estados['Mag'],auxEstadoActual['Mag']])
        estados['Phase'] = np.column_stack([estados['Phase'],auxEstadoActual['Phase']])
             
        
    YUnew           = calcYUfromEstados(obs['nodosU'],auxEstadoActual,rE)
    deltaA          = -np.matmul(np.matmul(np.linalg.inv(YT),(YUnew - YU)),A)
    Anew            = (A + deltaA) 

#<<<<<<<<<<<<<<<<<<<<< PLOT DE PRUEBA >>>>>>>>>>>>>>>>>>>>>>

#    idealMag = np.reshape(Videal['Mag'],(13,1))*np.ones((1,iteracionWLS))
#    idealPhase = np.reshape(Videal['Phase'],(13,1))*np.ones((1,iteracionWLS))
#    
#    plt.figure()    
#    for i in range(idealMag.shape[0]):
#        plt.subplot(2,7,i+1)
#        plt.plot(idealMag[i,:])
#        plt.plot(estados['Mag'][i,:],markersize=1)
#        plt.gca().set_title("Nodo: " + str(i+1))
#        plt.grid()    
#    
#    plt.figure()    
#    for i in range(idealMag.shape[0]):
#        plt.subplot(2,7,i+1)
#        plt.plot(idealPhase[i,:]*180/pi)
#        plt.plot(estados['Phase'][i,:]*180/pi,markersize=1)
#        plt.gca().set_title("Nodo: " + str(i+1))
#        plt.grid()    
#    
# 
#    
#    TVEMag  =  np.zeros(estados['Mag'].shape)
#    for i in range(estados['Mag'].shape[1]):
#        TVEMag[:,i] = (np.abs(Videal['Mag'] - estados['Mag'][:,i]))*100/np.abs(np.array(Videal['Mag']))
#    
#    
#    plt.figure()    
#    for i in range(TVEMag.shape[0]):
#        plt.subplot(2,7,i+1)
#        plt.plot(TVEMag[i,:])
#        plt.gca().set_title("Nodo: " + str(i+1))
#        plt.grid()    
#    
#        
#    TVEPhase   =  np.zeros(estados['Phase'].shape)
#    for i in range(estados['Phase'].shape[1]):
#        TVEPhase[:,i] = (np.abs(Videal['Phase']*180/pi -  estados['Phase'][:,i]*180/pi))*100/np.abs(Videal['Phase']*180/pi)
#    
#    plt.figure()    
#    for i in range(TVEPhase.shape[0]):
#        plt.subplot(2,7,i+1)
#        plt.plot(TVEPhase[i,:])
#        plt.gca().set_title("Nodo: " + str(i+1))
#        plt.grid()   

#<<<<<<<<<<<<<<<<<<<<< END PLOT DE PRUEBA >>>>>>>>>>>>>>>>>>>>>>

   
    return [Anew,auxEstadoActual]
    

def genMed(obs,rE,rE_falla,signal,TVEPMU=0,TVESCADA=0,intervaloPMU=0,intervaloSCADA=0):
    #El cuarto parámetro es el % de TVE de la med con respecto al ideal.
    Med                 = {}
    TVEPMU              = float(TVEPMU)
    TVESCADA            = float(TVESCADA)
    factorNoisePMU      = TVEPMU*(1/(3*np.sqrt(2)*100))
    factorNoiseSCADA    = TVESCADA*(1/(3*np.sqrt(2)*100))
    
    cantMuestras        = signal['bus_v'].shape[1]   
    cantNodos           = signal['bus_v'].shape[0]
    tiempo              = signal['baseTiempo']
    
    if intervaloPMU == 0:
        intervaloPMU = signal['baseTiempo'][1]      
    if intervaloSCADA == 0:
        intervaloSCADA = 125*intervaloPMU
     
    
    # VALORES IDEALES
    
    V       = signal['bus_v']
    I       = np.zeros((rE.cantNodos,cantMuestras),complex)
    IF      = np.zeros(cantMuestras)
    IT      = np.zeros(cantMuestras)
    PF      = np.zeros(cantMuestras)
    QF      = np.zeros(cantMuestras)
    PT      = np.zeros(cantMuestras)
    QT      = np.zeros(cantMuestras)
    Pinj    = np.zeros((rE.cantNodos,cantMuestras),complex)
    Qinj    = np.zeros((rE.cantNodos,cantMuestras),complex)
    
    for rama in rE.RamaLst:
        fr = rama['from']
        to = rama['to']
        IFaux = V[fr-1,:]*rama['Ybr'][0][0] + V[to-1,:]*rama['Ybr'][0][1]
        ITaux = V[fr-1,:]*rama['Ybr'][1][0] + V[to-1,:]*rama['Ybr'][1][1]
        I[fr-1,:] = I[fr-1,:] + IFaux
        I[to-1,:] = I[to-1,:] + ITaux
        PFaux = (np.conj(IFaux)*V[fr-1,:]).real
        QFaux = (np.conj(IFaux)*V[fr-1,:]).imag
        PTaux = (np.conj(ITaux)*V[to-1,:]).real
        QTaux = (np.conj(ITaux)*V[to-1,:]).imag       
        Pinj[fr-1,:] = Pinj[fr-1,:] + PFaux
        Pinj[to-1,:] = Pinj[to-1,:] + PTaux
        Qinj[fr-1,:] = Qinj[fr-1,:] + QFaux
        Qinj[to-1,:] = Qinj[to-1,:] + QTaux
        IF = np.row_stack([IF,IFaux])
        IT = np.row_stack([IT,ITaux])
        PF = np.row_stack([PF,PFaux]) 
        QF = np.row_stack([QF,QFaux])         
        PT = np.row_stack([PT,PTaux]) 
        QT = np.row_stack([QT,QTaux])
        
    IF = IF[1:,:]
    IT = IT[1:,:]
    PF = PF[1:,:]
    QF = QF[1:,:]
    PT = PT[1:,:]
    QT = QT[1:,:]




    yshunt = np.array([nodo['gshunt']+ 1j*nodo['bshunt'] for nodo in rE.NodoLst])
    yshunt_falla = np.array([nodo['gshunt']+ 1j*nodo['bshunt'] for nodo in rE_falla.NodoLst])
    
    np.zeros(cantMuestras)
        
    ISH    = np.zeros(cantNodos)
    PSH    = np.zeros(cantNodos)
    QSH    = np.zeros(cantNodos)
 
    for j in range(cantMuestras):
        ISHaux      = V[:,j]*yshunt
        PSHaux      = (np.conj(ISHaux)*V[:,j]).real
        QSHaux      = (np.conj(ISHaux)*V[:,j]).imag
        
        ISH = np.column_stack([ISH,ISHaux])
        PSH = np.column_stack([PSH,PSHaux])
        QSH = np.column_stack([QSH,QSHaux])
  
    ISH = ISH[:,1:]
    PSH = PSH[:,1:]
    QSH = QSH[:,1:]

#############################################################
# INCORPORO LA FALLA EN LA RED EN EL CALCULO DE MEDICIONES
    ISH_auxFalla    = np.zeros(cantNodos)
    PSH_auxFalla    = np.zeros(cantNodos)
    QSH_auxFalla    = np.zeros(cantNodos)
    
    for j in range(cantMuestras):
        if j>5 and j<10:
            ISHaux      = V[:,j]*yshunt_falla
            PSHaux      = (np.conj(ISHaux)*V[:,j]).real
            QSHaux      = (np.conj(ISHaux)*V[:,j]).imag
            
            ISH_auxFalla = np.column_stack([ISH_auxFalla,ISHaux])
            PSH_auxFalla = np.column_stack([PSH_auxFalla,PSHaux])
            QSH_auxFalla = np.column_stack([QSH_auxFalla,QSHaux])
  
    ISH_auxFalla = ISH_auxFalla[:,1:]
    PSH_auxFalla = PSH_auxFalla[:,1:]
    QSH_auxFalla = QSH_auxFalla[:,1:]
    
    ISH[:,6:10] = ISH_auxFalla
    PSH[:,6:10] = PSH_auxFalla
    QSH[:,6:10] = QSH_auxFalla
#############################################################
    
    I = I + ISH
    Pinj = Pinj + PSH
    Qinj = Qinj + QSH
    
    # MEDICIONES PMU
    # Tension
    MedPMU_V       = np.delete(V,(obs['nodosU']-1).astype(int),axis=0)
    noisePMU_V      = factorNoisePMU*np.abs(MedPMU_V)*np.random.randn(MedPMU_V.shape[0],MedPMU_V.shape[1]) 
    MedPMU_V       = np.add(MedPMU_V,noisePMU_V)    
    MedPMU_VMag    = np.abs(MedPMU_V)
    MedPMU_VPhase  = np.angle(MedPMU_V)
    # Corriente Shunt
    MedPMU_ISH     = np.delete(ISH,(obs['nodosU']-1).astype(int),axis=0)
    noisePMU_ISH   = factorNoisePMU*np.abs(MedPMU_ISH)*np.random.randn(MedPMU_ISH.shape[0],MedPMU_ISH.shape[1]) 
    MedPMU_ISH     = np.add(MedPMU_ISH,noisePMU_ISH)
    # Corriente Ramas
    MedPMU_IF      = np.delete(IF,(obs['ramasF_NoObs']-1).astype(int),axis=0)
    MedPMU_IT      = np.delete(IT,(obs['ramasT_NoObs']-1).astype(int),axis=0)
    noisePMUIF     = factorNoisePMU*np.abs(MedPMU_IF)*np.random.randn(MedPMU_IF.shape[0],MedPMU_IF.shape[1])
    noisePMUIT     = factorNoisePMU*np.abs(MedPMU_IT)*np.random.randn(MedPMU_IT.shape[0],MedPMU_IT.shape[1])
    MedPMU_IF      = np.add(MedPMU_IF,noisePMUIF)
    MedPMU_IT      = np.add(MedPMU_IT,noisePMUIT)
    
    
    # MEDICIONES SCADA
    # Tension
    MedSCADA_VMag     = np.abs(V)
    noiseSCADA_VMag   = factorNoiseSCADA*np.abs(MedSCADA_VMag)*np.random.randn(MedSCADA_VMag.shape[0],MedSCADA_VMag.shape[1]) 
    MedSCADA_VMag     = np.add(MedSCADA_VMag,noiseSCADA_VMag)
    # Corriente Shunt
    MedSCADA_ISHMag   = np.abs(ISH)
    noiseSCADA_ISHMag   = factorNoiseSCADA*np.abs(MedSCADA_ISHMag)*np.random.randn(MedSCADA_ISHMag.shape[0],MedSCADA_ISHMag.shape[1]) 
    MedSCADA_ISHMag     = np.add(MedSCADA_ISHMag,noiseSCADA_ISHMag)
    
    # Corriente Ramas
    MedSCADA_IFMag    = np.abs(IF)
    noiseSCADA_IFMag  = factorNoiseSCADA*np.abs(MedSCADA_IFMag)*np.random.randn(MedSCADA_IFMag.shape[0],MedSCADA_IFMag.shape[1]) 
    MedSCADA_IFMag    = np.add(MedSCADA_IFMag,noiseSCADA_IFMag)
    MedSCADA_ITMag    = np.abs(IT)
    noiseSCADA_ITMag  = factorNoiseSCADA*np.abs(MedSCADA_ITMag)*np.random.randn(MedSCADA_ITMag.shape[0],MedSCADA_ITMag.shape[1]) 
    MedSCADA_ITMag    = np.add(MedSCADA_ITMag,noiseSCADA_ITMag)    
    
   
    # Potencias Shunt
    MedSCADA_PSH      = PSH
    noiseSCADA_PSH    = factorNoiseSCADA*np.abs(MedSCADA_PSH)*np.random.randn(MedSCADA_PSH.shape[0],MedSCADA_PSH.shape[1]) 
    MedSCADA_PSH    = np.add(MedSCADA_PSH,noiseSCADA_PSH)
   
    MedSCADA_QSH      = QSH
    noiseSCADA_QSH    = factorNoiseSCADA*np.abs(MedSCADA_QSH)*np.random.randn(MedSCADA_QSH.shape[0],MedSCADA_QSH.shape[1]) 
    MedSCADA_QSH      = np.add(MedSCADA_QSH,noiseSCADA_QSH)
    
    
    # Potencias Ramas
    MedSCADA_PF       = PF
    noiseSCADA_PF     = factorNoiseSCADA*np.abs(MedSCADA_PF)*np.random.randn(MedSCADA_PF.shape[0],MedSCADA_PF.shape[1]) 
    MedSCADA_PF    = np.add(MedSCADA_PF,noiseSCADA_PF)

    MedSCADA_QF       = QF
    noiseSCADA_QF     = factorNoiseSCADA*np.abs(MedSCADA_QF)*np.random.randn(MedSCADA_QF.shape[0],MedSCADA_QF.shape[1]) 
    MedSCADA_QF    = np.add(MedSCADA_QF,noiseSCADA_QF)

    MedSCADA_PT       = PT
    noiseSCADA_PT     = factorNoiseSCADA*np.abs(MedSCADA_PT)*np.random.randn(MedSCADA_PT.shape[0],MedSCADA_PT.shape[1]) 
    MedSCADA_PT    = np.add(MedSCADA_PT,noiseSCADA_PT)

    MedSCADA_QT       = QT
    noiseSCADA_QT     = factorNoiseSCADA*np.abs(MedSCADA_QT)*np.random.randn(MedSCADA_QT.shape[0],MedSCADA_QT.shape[1]) 
    MedSCADA_QT    = np.add(MedSCADA_QT,noiseSCADA_QT)    

       
    MedSCADA_Pinj      = Pinj
    noiseSCADA_Pinj    = factorNoiseSCADA*np.abs(MedSCADA_Pinj)*np.random.randn(MedSCADA_Pinj.shape[0],MedSCADA_Pinj.shape[1]) 
    MedSCADA_Pinj    = np.add(MedSCADA_Pinj,noiseSCADA_Pinj)
   
    MedSCADA_Qinj      = Qinj
    noiseSCADA_Qinj    = factorNoiseSCADA*np.abs(MedSCADA_Qinj)*np.random.randn(MedSCADA_Qinj.shape[0],MedSCADA_Qinj.shape[1]) 
    MedSCADA_Qinj    = np.add(MedSCADA_Qinj,noiseSCADA_Qinj)   
     

    
    # Adecuacion Segun Intervalo de Medicion SCADA
 
    for j in range(cantMuestras):
        if j == 0 or j%np.int(intervaloSCADA/intervaloPMU)==0:
            MedSCADA_Pinj_aux = MedSCADA_Pinj[:,j]
            MedSCADA_Qinj_aux = MedSCADA_Qinj[:,j]
            MedSCADA_VMag_aux = MedSCADA_VMag[:,j]
            MedSCADA_ISHMag_aux = MedSCADA_ISHMag[:,j]
            MedSCADA_IFMag_aux = MedSCADA_IFMag[:,j]
            MedSCADA_ITMag_aux = MedSCADA_ITMag[:,j]
            MedSCADA_PF_aux = MedSCADA_PF[:,j]
            MedSCADA_QF_aux = MedSCADA_QF[:,j]
            MedSCADA_PT_aux = MedSCADA_PT[:,j]
            MedSCADA_QT_aux = MedSCADA_QT[:,j]
            MedSCADA_PSH_aux = MedSCADA_PSH[:,j]
            MedSCADA_QSH_aux = MedSCADA_QSH[:,j]
        
        MedSCADA_Pinj[:,j] = MedSCADA_Pinj_aux
        MedSCADA_Qinj[:,j] = MedSCADA_Qinj_aux 
        MedSCADA_VMag[:,j] = MedSCADA_VMag_aux
        MedSCADA_ISHMag[:,j] = MedSCADA_ISHMag_aux
        MedSCADA_IFMag[:,j] = MedSCADA_IFMag_aux
        MedSCADA_ITMag[:,j] = MedSCADA_ITMag_aux
        MedSCADA_PF[:,j] = MedSCADA_PF_aux
        MedSCADA_QF[:,j] = MedSCADA_QF_aux
        MedSCADA_PT[:,j] = MedSCADA_PT_aux
        MedSCADA_QT[:,j] = MedSCADA_QT_aux
        MedSCADA_PSH[:,j] = MedSCADA_PSH_aux
        MedSCADA_QSH[:,j] = MedSCADA_QSH_aux
    
    
    Med['tiempo']         = tiempo
    Med['cantMuestras'] = cantMuestras   
    Med['Ideal_V']      = np.round(V,12)
    Med['Ideal_I']      = np.round(I,12)
    Med['Ideal_IF']     = np.round(IF,12)
    Med['Ideal_IT']     = np.round(IT,12)
    Med['Ideal_ISH']    = np.round(ISH,12)
    Med['Ideal_ISH_falla'] = np.round(ISH_auxFalla,12)
    Med['Ideal_PF']     = np.round(PF,12)
    Med['Ideal_QF']     = np.round(QF,12)
    Med['Ideal_PT']     = np.round(PT,12)
    Med['Ideal_QT']     = np.round(QT,12)
    Med['Ideal_PSH']    = np.round(PSH,12)
    Med['Ideal_QSH']    = np.round(QSH,12)
    Med['Ideal_Pinj']   = np.round(np.real(Pinj),12)
    Med['Ideal_Qinj']   = np.round(np.real(Qinj),12)
     
    
    Med['PMU_V']        = MedPMU_V
    Med['PMU_VMag']     = MedPMU_VMag
    Med['PMU_VPhase']   = MedPMU_VPhase
    Med['PMU_ISH']      = MedPMU_ISH
    Med['PMU_IF']       = MedPMU_IF
    Med['PMU_IFMag']    = np.abs(MedPMU_IF)
    Med['PMU_IFPhase']  = np.angle(MedPMU_IF)  # Ver si en radianes o en angulo        
    Med['PMU_IT']       = MedPMU_IT    
    Med['PMU_ITMag']    = np.abs(MedPMU_IT)
    Med['PMU_ITPhase']  = np.angle(MedPMU_IT)  # Ver si en radianes o en angulo        


    
    Med['SCADA_VMag']   = MedSCADA_VMag
    Med['SCADA_ISHMag'] = MedSCADA_ISHMag
    Med['SCADA_IFMag']  = MedSCADA_IFMag
    Med['SCADA_ITMag']  = MedSCADA_ITMag
    Med['SCADA_PF']     = MedSCADA_PF
    Med['SCADA_QF']     = MedSCADA_QF   
    Med['SCADA_PT']     = MedSCADA_PT
    Med['SCADA_QT']     = MedSCADA_QT
    Med['SCADA_PSH']    = MedSCADA_PSH
    Med['SCADA_QSH']    = MedSCADA_QSH
    Med['SCADA_Pinj']   = MedSCADA_Pinj
    Med['SCADA_Qinj']   = MedSCADA_Qinj
    
    
    Med['Vec'] = np.append(MedSCADA_Pinj,MedSCADA_Qinj,axis=0) 
#    Med['Vec'] = np.append(Med['Vec'],MedSCADA_PF,axis=0)
#    Med['Vec'] = np.append(Med['Vec'],MedSCADA_PT,axis=0)    
#    Med['Vec'] = np.append(Med['Vec'],MedSCADA_QF,axis=0)    
#    Med['Vec'] = np.append(Med['Vec'],MedSCADA_QT,axis=0)
#    Med['Vec'] = np.append(Med['Vec'],MedSCADA_IFMag,axis=0)
#    Med['Vec'] = np.append(Med['Vec'],MedSCADA_ITMag,axis=0)
#    Med['Vec'] = np.append(Med['Vec'],MedSCADA_VMag,axis=0)
    Med['Vec'] = np.append(Med['Vec'],MedPMU_VMag,axis=0)
    Med['Vec'] = np.append(Med['Vec'],MedPMU_VPhase,axis=0)
#    Med['Vec'] = np.append(Med['Vec'],Med['PMU_IFMag'],axis=0)
#    Med['Vec'] = np.append(Med['Vec'],Med['PMU_IFPhase'],axis=0)
#    Med['Vec'] = np.append(Med['Vec'],Med['PMU_ITMag'],axis=0)
#    Med['Vec'] = np.append(Med['Vec'],Med['PMU_ITPhase'],axis=0)






# Potencias Inyectadas - CUANDO COMPRUEBES QUE NUEVO METODO PINJ/QINJ ANDA BORRAR
#    Pinj = np.zeros(cantMuestras)
#    Qinj = np.zeros(cantMuestras)
#    for i in range(cantNodos):
#        ramasF_delete = np.array([x['ramaN'] for x in rE.RamaLst if x['from'] !=  i+1])
#        ramasT_delete = np.array([x['ramaN'] for x in rE.RamaLst if x['to'] !=  i+1])
#        
#        Pinj_aux = np.sum(np.delete(PF,(ramasF_delete-1).astype(int),axis=0),axis=0)
#        Pinj_aux = Pinj_aux + np.sum(np.delete(PT,(ramasT_delete-1).astype(int),axis=0),axis=0)
#        Qinj_aux = np.sum(np.delete(QF,(ramasF_delete-1).astype(int),axis=0),axis=0)
#        Qinj_aux = Qinj_aux + np.sum(np.delete(QT,(ramasT_delete-1).astype(int),axis=0),axis=0)
#        
#        Pinj = np.row_stack([Pinj,Pinj_aux])
#        Qinj = np.row_stack([Qinj,Qinj_aux])        
#    
#    Pinj = Pinj[1:,:]
#    Qinj = Qinj[1:,:]    
#    Pinj = Pinj + PSH
#    Qinj = Qinj + QSH
# 
#    for i in range(cantNodos):
#        ramasF_delete = np.array([x['ramaN'] for x in rE.RamaLst if x['from'] !=  i+1])
#        ramasT_delete = np.array([x['ramaN'] for x in rE.RamaLst if x['to'] !=  i+1])
#
#        Pinj_aux = np.sum(np.delete(MedSCADA_PF,(ramasF_delete-1).astype(int),axis=0),axis=0)
#        Pinj_aux = Pinj_aux + np.sum(np.delete(MedSCADA_PT,(ramasT_delete-1).astype(int),axis=0),axis=0)
#        Qinj_aux = np.sum(np.delete(MedSCADA_QF,(ramasF_delete-1).astype(int),axis=0),axis=0)
#        Qinj_aux = Qinj_aux + np.sum(np.delete(MedSCADA_QT,(ramasT_delete-1).astype(int),axis=0),axis=0)
#        
#        MedSCADA_Pinj = np.row_stack([MedSCADA_Pinj,Pinj_aux])
#        MedSCADA_Qinj = np.row_stack([MedSCADA_Qinj,Qinj_aux])        
#           
#    MedSCADA_Pinj = MedSCADA_Pinj[1:,:]
#    MedSCADA_Qinj = MedSCADA_Qinj[1:,:]    
#    MedSCADA_Pinj = MedSCADA_Pinj + MedSCADA_PSH
#    MedSCADA_Qinj = MedSCADA_Qinj + MedSCADA_QSH

       
    return Med


def plotAllPhasors(mediciones,obs,estadosSinWLS,estadosWLS,cantMuestras=0):
    
    if cantMuestras == 0:
        cantMuestras = mediciones['cantMediciones']
        print(cantMuestras)  
    cantNodos = mediciones['Ideal_V'].shape[0]
    cantRamas = mediciones['Ideal_IF'].shape[0]
    
    tiempo = mediciones['tiempo'][0:cantMuestras]
    
    #IDEAL
    fasoresVIdeal   = mediciones['Ideal_V'][:,0:cantMuestras] 
    fasoresISHIdeal = mediciones['Ideal_ISH'][:,0:cantMuestras] 
    fasoresIFIdeal = mediciones['Ideal_IF'][:,0:cantMuestras]    
    fasoresITPMU   = mediciones['PMU_IT'][:,0:cantMuestras]  
    PFIdeal        = mediciones['Ideal_PF'][:,0:cantMuestras]  
    QFIdeal        = mediciones['Ideal_QF'][:,0:cantMuestras]  
    PTIdeal        = mediciones['Ideal_PT'][:,0:cantMuestras]  
    QTIdeal        = mediciones['Ideal_QT'][:,0:cantMuestras]   
    
    
    #PMU
    fasoresVMedPMU  = mediciones['PMU_V'][:,0:cantMuestras]  
    fasoresISHPMU   = mediciones['PMU_ISH'][:,0:cantMuestras] 
    fasoresIFPMU   = mediciones['PMU_IF'][:,0:cantMuestras]
    fasoresITIdeal = mediciones['Ideal_IT'][:,0:cantMuestras]

    #SCADA
    VMagSCADA   = mediciones['SCADA_VMag'][:,0:cantMuestras]
    ISHMagSCADA = mediciones['SCADA_ISHMag'][:,0:cantMuestras]
    IFMagSCADA  = mediciones['SCADA_IFMag'][:,0:cantMuestras]
    ITMagSCADA  = mediciones['SCADA_ITMag'][:,0:cantMuestras]
    PFSCADA     = mediciones['SCADA_PF'][:,0:cantMuestras]
    QFSCADA     = mediciones['SCADA_QF'][:,0:cantMuestras]
    PTSCADA     = mediciones['SCADA_PT'][:,0:cantMuestras]
    QTSCADA     = mediciones['SCADA_QT'][:,0:cantMuestras]
    
    # Estimacion Estados
    fasorEstadosWLS    = estadosWLS[:,0:cantMuestras]
    fasorEstadosSinWLS = estadosSinWLS[:,0:cantMuestras]

    
## TENSION
    
#### Magnitud  
    plt.figure()
    plt.suptitle('Modulo de Tension')
    ii = 0
    for i in range(cantNodos):       
        plt.subplot(2,7,i+1)
        plt.plot(tiempo,np.abs(fasoresVIdeal[i,:]))
        plt.plot(tiempo,VMagSCADA[i,:],'mx',markersize=3)
        plt.plot(tiempo,np.abs(fasorEstadosWLS[i,:]),'k*',markersize=5)
        plt.plot(tiempo,np.abs(fasorEstadosSinWLS[i,:]),'go',markersize=3)        
        if i+1 in obs['nodosO']:
            plt.plot(tiempo,np.abs(fasoresVMedPMU[ii,:]),'.',markersize=3)
            ii = ii +1
        plt.gca().set_title("Nodo: " + str(i+1))
        plt.gca().set_ylim([0,1.1])
        plt.grid()
        
#### Fase
    plt.figure()
    plt.suptitle('Fase de Tension PMU')
    ii = 0
    for i in range(cantNodos):       
        plt.subplot(2,7,i+1)
        plt.plot(tiempo,np.angle(fasoresVIdeal[i,:])*180/pi)
        plt.plot(tiempo,np.angle(fasorEstadosWLS[i,:])*180/pi,'k*',markersize=5)
        plt.plot(tiempo,np.angle(fasorEstadosSinWLS[i,:])*180/pi,'go',markersize=3)
        if i+1 in obs['nodosO']:
            plt.plot(tiempo,np.angle(fasoresVMedPMU[ii,:])*180/pi,'.')
            ii = ii +1
        plt.gca().set_ylim([-35,60])
        plt.gca().set_title("Nodo: " + str(i+1))
        plt.grid()
        
## CORRIENTES
    plt.figure()
    plt.suptitle('Modulo de Corrientes Shunt')  
    ii = 0 
    for i in range(cantNodos):     
        plt.subplot(2,7,i+1)
        plt.plot(tiempo,np.abs(fasoresISHIdeal[i,:]))
        plt.plot(tiempo,ISHMagSCADA[i,:],'mx',markersize=3)
        if i+1 in obs['nodosO']:
            plt.plot(tiempo,np.abs(fasoresISHPMU[ii,:]),'.')
            ii = ii +1
        plt.gca().set_title("Nodo: " + str(i+1))
        plt.grid()    
        
        
    plt.figure()
    plt.suptitle('Fases de Corrientes Shunt')  
    ii = 0 
    for i in range(cantNodos):     
        plt.subplot(2,7,i+1)
        plt.plot(tiempo,np.angle(fasoresISHIdeal[i,:])*180/pi)
        if i+1 in obs['nodosO']:
            plt.plot(tiempo,np.angle(fasoresISHPMU[ii,:])*180/pi,'.')
            ii = ii +1
        plt.gca().set_title("Nodo: " + str(i+1))
        plt.grid()        
        
    plt.figure()
    plt.suptitle('Modulo de Corrientes From')  
    ii = 0 
    for i in range(cantRamas):     
        plt.subplot(3,5,i+1)
        plt.plot(tiempo,np.abs(fasoresIFIdeal[i,:]))
        plt.plot(tiempo,IFMagSCADA[i,:],'mx',markersize=3)
        if i+1 in obs['ramasF_Obs']:
            plt.plot(tiempo,np.abs(fasoresIFPMU[ii,:]),'.')
            ii = ii +1
        plt.grid()
 
    plt.figure()
    plt.suptitle('Fase de Corrientes From')  
    ii = 0 
    for i in range(cantRamas):     
        plt.subplot(3,5,i+1)
        plt.plot(tiempo,np.angle(fasoresIFIdeal[i,:])*180/pi)
        if i+1 in obs['ramasF_Obs']:
            plt.plot(tiempo,np.angle(fasoresIFPMU[ii,:])*180/pi,'.')
            ii = ii +1
        plt.grid()

     
    plt.figure()
    plt.suptitle('Modulo de Corrientes To')  
    ii = 0 
    for i in range(cantRamas):     
        plt.subplot(3,5,i+1)
        plt.plot(tiempo,np.abs(fasoresITIdeal[i,:]))
        plt.plot(tiempo,ITMagSCADA[i,:],'mx',markersize=3)
        if i+1 in obs['ramasT_Obs']:
            plt.plot(tiempo,np.abs(fasoresITPMU[ii,:]),'.')
            ii = ii +1
        plt.grid()        
        
    plt.figure()
    plt.suptitle('Fase de Corrientes To')  
    ii = 0 
    for i in range(cantRamas):     
        plt.subplot(3,5,i+1)
        plt.plot(tiempo,np.angle(fasoresITIdeal[i,:])*180/pi)
        if i+1 in obs['ramasT_Obs']:
            plt.plot(tiempo,np.angle(fasoresITPMU[ii,:])*180/pi,'.')
            ii = ii +1
        plt.grid()        
              


## POTENCIAS 


    plt.figure()
    plt.suptitle('Potencia PFr Ramas')  
    ii = 0 
    for i in range(cantRamas):     
        plt.subplot(3,5,i+1)
        plt.plot(tiempo,PFIdeal[i,:])
        plt.plot(tiempo,PFSCADA[i,:],'mx',markersize=3)
        plt.grid()           
        
    plt.figure()
    plt.suptitle('Potencia QFr Ramas')  
    ii = 0 
    for i in range(cantRamas):     
        plt.subplot(3,5,i+1)
        plt.plot(tiempo,QFIdeal[i,:])
        plt.plot(tiempo,QFSCADA[i,:],'mx',markersize=3)
        plt.grid()           
        
    plt.figure()
    plt.suptitle('Potencia PTo Ramas')  
    ii = 0 
    for i in range(cantRamas):     
        plt.subplot(3,5,i+1)
        plt.plot(tiempo,PTIdeal[i,:])
        plt.plot(tiempo,PTSCADA[i,:],'mx',markersize=3)
        plt.grid()          
        
    plt.figure()
    plt.suptitle('Potencia QTo Ramas')  
    ii = 0 
    for i in range(cantRamas):     
        plt.subplot(3,5,i+1)
        plt.plot(tiempo,QTIdeal[i,:])
        plt.plot(tiempo,QTSCADA[i,:],'mx',markersize=3)
        plt.grid()           
        
   
        
def genMedSCADA(rE,TVEPerc=0):
    #El 2do parametro es el % de TVE de la med     
    Med = {}
    TVEPerc = float(TVEPerc)
    factorNoise = TVEPerc*(1/(3*np.sqrt(2)*100))
    
    Med_Pinj = [nodo['Pinj'] for nodo in rE.NodoLst]
    Med_Qinj = [nodo['Qinj'] for nodo in rE.NodoLst]
    Med_PflowF = [rama['PFr'] for rama in rE.RamaLst]
    Med_QflowF = [rama['QFr'] for rama in rE.RamaLst]
    Med_PflowT = [rama['PTo'] for rama in rE.RamaLst]
    Med_QflowT = [rama['QTo'] for rama in rE.RamaLst]
    Med_IMagF = [np.absolute(rama['IF']) for rama in rE.RamaLst]
    Med_IMagT = [np.absolute(rama['IT']) for rama in rE.RamaLst]
    Med_VMag = [nodo['VMod'] for nodo in rE.NodoLst]    
    

    noiseAux = factorNoise*np.max(np.abs(Med_Pinj))*np.random.randn(len(Med_Pinj))
    Med_Pinj = np.add(Med_Pinj,noiseAux)
    noiseAux = factorNoise*np.max(np.abs(Med_Qinj))*np.random.randn(len(Med_Qinj))
    Med_Qinj = np.add(Med_Qinj,noiseAux)
    noiseAux = factorNoise*np.max(np.abs(Med_PflowF))*np.random.randn(len(Med_PflowF))
    Med_PflowF = np.add(Med_PflowF,noiseAux)
    noiseAux = factorNoise*np.max(np.abs(Med_QflowF))*np.random.randn(len(Med_QflowF))
    Med_QflowF = np.add(Med_QflowF,noiseAux)
    noiseAux = factorNoise*np.max(np.abs(Med_PflowT))*np.random.randn(len(Med_PflowT))
    Med_PflowT = np.add(Med_PflowT,noiseAux)
    noiseAux = factorNoise*np.max(np.abs(Med_QflowT))*np.random.randn(len(Med_QflowT))
    Med_QflowT = np.add(Med_QflowT,noiseAux)
    noiseAux = factorNoise*np.max(np.abs(Med_IMagF))*np.random.randn(len(Med_IMagF))
    Med_IMagF = np.add(Med_IMagF,noiseAux)
    noiseAux = factorNoise*np.max(np.abs(Med_IMagT))*np.random.randn(len(Med_IMagT))
    Med_IMagT = np.add(Med_IMagT,noiseAux)
    noiseAux = factorNoise*np.max(np.abs(Med_VMag))*np.random.randn(len(Med_VMag))
    Med_VMag = np.add(Med_VMag,noiseAux)
      
    
    Med['Pinj'] = Med_Pinj
    Med['Qinj'] = Med_Qinj
    Med['PflowF'] = Med_PflowF
    Med['QflowF'] = Med_QflowF
    Med['PflowT'] = Med_PflowT
    Med['QflowT'] = Med_QflowT
#    Med['IMagF'] = Med_IMagF
#    Med['IMagT'] = Med_IMagT
    Med['VMag'] = Med_VMag    
    
    Med['Vec'] = np.append(Med_Pinj,Med_Qinj)
#    Med['Vec'] = np.append(Med['Vec'],Med_PflowF)
#    Med['Vec'] = np.append(Med['Vec'],Med_PflowT)
#    Med['Vec'] = np.append(Med['Vec'],Med_QflowF)
#    Med['Vec'] = np.append(Med['Vec'],Med_QflowT)
#    Med['Vec'] = np.append(Med['Vec'],Med_IMagF)
#    Med['Vec'] = np.append(Med['Vec'],Med_IMagT)
    Med['Vec'] = np.append(Med['Vec'],Med_VMag)

    return Med

def genMedPMU(nodosO,nodosU,rE,TVEPerc=0):
    #El cuarto parámetro es el % de TVE de la med con respecto al ideal.
    Med = {}
    TVEPerc = float(TVEPerc)
    factorNoise = TVEPerc*(1/(3*np.sqrt(2)*100))
   
    Med_V       = np.array([nodo['V'] for nodo in rE.NodoLst if (nodo['nodoN'] in nodosO)])
    Med_VMag    = np.array([nodo['VMod'] for nodo in rE.NodoLst if (nodo['nodoN'] in nodosO)])
    Med_VPhase  = np.array([nodo['VFas'] for nodo in rE.NodoLst if (nodo['nodoN'] in nodosO)])# El elemento [0] es la referencia por eso lo salteo
    Med_IFMag   = np.absolute([rama['IF'] for rama in rE.RamaLst if (rama['from'] in nodosO)])
    Med_IFPhase = np.angle([rama['IF'] for rama in rE.RamaLst if (rama['from'] in nodosO)])
    Med_ITMag   = np.absolute([rama['IT'] for rama in rE.RamaLst if (rama['to'] in nodosO)])
    Med_ITPhase = np.angle([rama['IT'] for rama in rE.RamaLst if (rama['to'] in nodosO)] )
      
    noiseAux    = factorNoise*np.abs(Med_V)*(np.random.randn(len(Med_V))+1j*np.random.randn(len(Med_V)))
    Med_V       = np.add(Med_V,noiseAux)
    Med_VMag    = np.abs(Med_V)
    Med_VPhase  = np.angle(Med_V)
#    noiseAux = factorNoise*np.abs(Med_VMag)*np.random.randn(len(Med_VMag))
#    Med_VMag = np.add(Med_VMag,noiseAux)
#    noiseAux = factorNoise*np.abs(Med_VPhase)*np.random.randn(len(Med_VPhase))
#    Med_VPhase = np.add(Med_VPhase,noiseAux)
    noiseAux = factorNoise*np.abs(Med_IFMag)*np.random.randn(len(Med_IFMag))
    Med_IFMag = np.add(Med_IFMag,noiseAux)
    noiseAux = factorNoise*np.abs(Med_IFPhase)*np.random.randn(len(Med_IFPhase))
    Med_IFPhase = np.add(Med_IFPhase,noiseAux)
    noiseAux = factorNoise*np.abs(Med_ITMag)*np.random.randn(len(Med_ITMag))
    Med_ITMag = np.add(Med_ITMag,noiseAux)
    noiseAux = factorNoise*np.abs(Med_ITPhase)*np.random.randn(len(Med_ITPhase))
    Med_ITPhase = np.add(Med_ITPhase,noiseAux)    
    
    Med['V']        = Med_V
    Med['VMag']     = Med_VMag
    Med['VPhase']   = Med_VPhase
    Med['IFMag']    = Med_IFMag
    Med['IFPhase']  = Med_IFPhase
    Med['ITMag']    = Med_ITMag
    Med['ITPhase']  = Med_ITPhase
    

#    Med['Vec'] = Med_VMag
    Med['Vec'] = np.append(Med_VMag,Med_VPhase)
#    Med['Vec'] = np.append(Med['Vec'],Med_IFMag)    
#    Med['Vec'] = np.append(Med['Vec'],Med_IFPhase)
#    Med['Vec'] = np.append(Med['Vec'],Med_ITMag)
#    Med['Vec'] = np.append(Med['Vec'],Med_IFPhase)
#   
    return Med       