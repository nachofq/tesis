function mpc = case_13bus
%CASE9    Power flow data for 9 bus, 3 generator case.
%   Please see CASEFORMAT for details on the case file format.
%
%   Based on data from Joe H. Chow's book, p. 70.

%   MATPOWER

%% MATPOWER Case Format : Version 2
mpc.version = '2';

%%-----  Power Flow Data  -----%%
%% system MVA base
mpc.baseMVA = 100;

%% bus data
%	bus_i	type	Pd		Qd	Gs	Bs	area	Vm		Va		baseKV	zone	Vmax	Vmin
mpc.bus = [
	1		3		0		0	0	0	1		1.03	18.5	345		1		1.1		0.9;
	2		2		0		0	0	0	1		1.01	8.80	345		1		1.1		0.9;
	3		1		0		0	0	3	1		0.9781	-6.1	345		1		1.5		0.5;
	4		1		9.76	1	0	0	1		0.95	-10		345		1		1.05	0.95;
	5		1		0		0	0	0	1		1.0103	12.1	345		1		1.5		0.5;
	6		2		0		0	0	0	1		1.03	-6.8 	345		1		1.1		0.9;
	7		2		0		0	0	0	1		1.01	-16.9	345		1		1.1		0.9;
	8		1		0		0	0	5	1		0.9899	-31.8 	345		1		1.5		0.5;
	9		1		17.65	1	0	0	1		0.95	-35		345		1		1.05	0.95;
	10		1		0		0	0	0	1		0.9876	2.1		345		1		1.5		0.5;
	11		2		0		0	0	0	1		1		-19.3	345		1		1.5		0.5;
	12		1		0		0	0	0	1		1.0125	-13.4	345		1		1.5		0.5;
	13		1		0		0	0	0	1		0.9938	-23.6	345		1		1.5		0.5;
];

%% generator data
%	bus	Pg		Qg		Qmax	Qmin	Vg		mBase	status	Pmax	Pmin	Pc1	Pc2	Qc1min	Qc1max	Qc2min	Qc2max	ramp_agc	ramp_10	ramp_30	ramp_q	apf
mpc.gen = [
	1	7.00	1.61	5.0		-1		1.03	100		1		25		0.01	0	0	0		0		0		0		0			0		0		0		0;
	2	7.00 	1.76	5.0		-1		1.01	100		1		30		0.01	0	0	0		0		0		0		0			0		0		0		0;
	6	7.16	1.49	5.0		-1		1.03	100		1		27		0.01	0	0	0		0		0		0		0			0		0		0		0;
	7	7.00	1.39	5.0		-1		1.01	100		1		27		0.01	0	0	0		0		0		0		0			0		0		0		0;
	11	0.00	1.09	2		0		1		100		1		27		0.01	0	0	0		0		0		0		0			0		0		0		0;	
];

%% branch data
%	branch_i fbus	tbus	r		x		b		rateA	rateB	rateC	ratio	angle	status	angmin	angmax
mpc.branch = [
	1		1		5		0		0.0167	0		250		250		250		0		0		1		-360	360;
	2		2		10		0.0		0.0167	0.00	250		250		250		0		0		1		-360	360;
	3		3		4		0.0		0.005	0.00	150		150		150		0		0		1		-360	360;
	4		3		10		0.001	0.0100	0.0175	300		300		300		0		0		1		-360	360;
	5		3		11		0.011	0.110	0.1925	150		150		150		0		0		1		-360	360;
	6		3		11		0.011	0.110	0.1925	250		250		250 	0		0		1		-360	360;
	7		5		10		0.0025	0.025	0.0437	250		250		250		0		0		1		-360	360;
	8		6		12		0.0		0.0167	0.0		250		250		250		0		0		1		-360	360;
	9		7		13		0.0		0.0167	0.0		250		250		250		0		0		1		-360	360;
	10		8		11		0.011	0.11	0.1925	250		250		250		0		0		1		-360	360;
	11		8		11		0.011	0.11	0.1925	250		250		250		0		0		1		-360	360;
	12		8		9		0.0		0.005	0.00	250		250		250		0		0		1		-360	360;
	13		8		13		0.001	0.01	0.0175	250		250		250		0		0		1		-360	360;
	14		12		13		0.0025	0.025	0.0437	250		250		250		0		0		1		-360	360;
];

