from common import *

class RedElec:
    def __init__(self, case, pf_tol=1e-12, pf_max_it=100,flagMedicion=0):

# flagMedicion sirve para avisarle a red eléctrica si tiene que usar 
# la tensión de la resolución por iteraciones o las tensiones del case.
        
        
#        case = globals()[case]() Esta instruccion era para pasarle un string
#                                   en lugar de un objeto case.
        self.case = case
        opt = ppoption(PF_TOL=pf_tol, PF_MAX_IT=pf_max_it,VERBOSE=0, OUT_ALL=0)
        estado = runpf(case, opt)[0]
        if flagMedicion == 1:
            estado['bus'] = case['bus']  
        self.estado = estado
        
# Estado de los nodos
# [Numero TensionMod TensionFas TensionCompleja]       
        NodoLst = []
        for i in range(len(estado['bus'])):
            dict = {}
            dict['nodoN']  = i+1
#            dict['LoadP']  = estado['bus'][i][4]
#            dict['LoadQ']  = estado['bus'][i][5]
            dict['gshunt'] = estado['bus'][i][4] #/estado['baseMVA']  
            dict['bshunt'] = estado['bus'][i][5]#/estado['baseMVA']  
            dict['VMod']   = estado['bus'][i][7]
#            dict['VFas']   = estado['bus'][i][8]*pi/180
#            dict['V']      = dict['VMod']*np.exp(1j*dict['VFas'])
            dict['VFas']   = estado['bus'][i][8]
            dict['V']      = dict['VMod']*np.exp(1j*dict['VFas']*pi/180)
            try:
                dict['Pinj'] = ([x[1] for x in estado['gen'] if i+1 == x[0]] + (-estado['bus'][i][2]))[0]/estado['baseMVA']
            except:
                dict['Pinj'] = -estado['bus'][i][2]/estado['baseMVA']
            try:
                dict['Qinj'] = ([x[2] for x in estado['gen'] if i+1 == x[0]] + (-estado['bus'][i][3]))[0]/estado['baseMVA']
            except:
                dict['Qinj'] = -estado['bus'][i][3]/estado['baseMVA']           
            NodoLst.append(dict)
        self.NodoLst = NodoLst
        self.cantNodos = i+1
        
        
# Estado de las ramas
# [NumeroDeRama RamaFrom RamaTo Y B TapMod TapFas PF QF PT QT IF IT ]       
        RamaLst = []        
        for i in range(len(estado['branch'])):        
            dict = {}
            dict['ramaN'] = i+1
            dict['from'] = int(case['branch'][i][0]) 
            dict['to'] = int(case['branch'][i][1])
            dict['ys'] = 1/(case['branch'][i][2] + 1j*case['branch'][i][3])
            dict['B'] = case['branch'][i][4]
            dict['TapMod'] = case['branch'][i][8] + (case['branch'][i][8] == 0)
            dict['TapPhase'] = case['branch'][i][9]
            
            dict['Ybr'] = np.zeros((2,2),dtype=np.complex_)
            dict['Ybr'][0][0] = (dict['ys'] + 1j*dict['B']/2 )*(1/(dict['TapMod']**2)) #ff
            dict['Ybr'][0][1] = -dict['ys']*(1/(dict['TapMod']*e**(-1j*dict['TapPhase'])))#ft
            dict['Ybr'][1][0] = -dict['ys']*(1/(dict['TapMod']*e**(1j*dict['TapPhase'])))#tf
            dict['Ybr'][1][1] = (dict['ys'] + 1j*dict['B']/2 )#tt
            
            
            dict['PFr'] = estado['branch'][i][13]/estado['baseMVA']
            dict['QFr'] = estado['branch'][i][14]/estado['baseMVA']
            dict['PTo'] = estado['branch'][i][15]/estado['baseMVA']
            dict['QTo'] = estado['branch'][i][16]/estado['baseMVA']               
            dict['IF'] = ((dict['PFr'] + 1j*dict['QFr'])/ \
                [x['V'] for x in self.NodoLst if x['nodoN']==dict['from']][0]).conjugate()                
            dict['IF_Mag'] = np.absolute(dict['IF'])  
            dict['IF_Phase'] = np.angle(dict['IF'])
            dict['IT'] = ((dict['PTo'] + 1j*dict['QTo'])/ \
                [x['V'] for x in self.NodoLst if x['nodoN']==dict['to']][0]).conjugate()        
            dict['IT_Mag'] = np.absolute(dict['IT'])  
            dict['IT_Phase'] = np.angle(dict['IT'])            
            
            RamaLst.append(dict)
            
            
        self.RamaLst = RamaLst   
        self.cantRamas = i+1
        self.ramas = [np.append(r['ramaN'],np.append(r['from'],r['to'])) for r in self.RamaLst]
        self.Ikeys = list(self.RamaLst[0].keys())
        self.VKeys = list(self.NodoLst[0].keys())
        
        
        for i in range(len(estado['bus'])):
            aux = [x['to'] for x in self.RamaLst if x['from']== i+1] \
                            +[x['from'] for x in self.RamaLst if x['to']==i+1]
            aux.sort()
            self.NodoLst[i]['Vecinos'] = aux       
       
        #Matriz Y
        
        self.Ysh =  np.zeros((self.cantNodos,),dtype=np.complex_)
        for nodo in self.NodoLst:
            self.Ysh[nodo['nodoN']-1] =  nodo['gshunt']+1j*nodo['bshunt']
        
        self.CF = np.zeros((self.cantRamas,self.cantNodos))
        self.CT = np.zeros((self.cantRamas,self.cantNodos))
        self.Yff =np.zeros((self.cantRamas,),dtype=np.complex_)
        self.Yft =np.zeros((self.cantRamas,),dtype=np.complex_)
        self.Ytf =np.zeros((self.cantRamas,),dtype=np.complex_)
        self.Ytt =np.zeros((self.cantRamas,),dtype=np.complex_)
        
        
        for rama in self.RamaLst:
            self.CF[rama['ramaN']-1][rama['from']-1]=1
            self.CT[rama['ramaN']-1][rama['to']-1]= 1
            self.Yff[rama['ramaN']-1] = rama['Ybr'][0][0]
            self.Yft[rama['ramaN']-1] = rama['Ybr'][0][1]
            self.Ytf[rama['ramaN']-1] = rama['Ybr'][1][0]
            self.Ytt[rama['ramaN']-1] = rama['Ybr'][1][1]
            
        self.Yf =  np.matmul(np.diag(self.Yff),self.CF)+np.matmul(np.diag(self.Yft),self.CT)
        self.Yt =  np.matmul(np.diag(self.Ytf),self.CF)+np.matmul(np.diag(self.Ytt),self.CT)
        # A la matriz Y le saqué Ys. Matpower la incluye pero lo que noté es que
        # El calculo de las corrientes a partir de las potencias y tensión 
        # da lo mismo que Y*V cuando la matriz Y carece de Ys.
        # Será que al cálculo de corrientes tengo que además incluir la potencia
        # Derivada en la carga? Seems legit.
        self.Y = np.matmul(np.matrix.transpose(self.CF),self.Yf)+np.matmul(np.matrix.transpose(self.CT),self.Yt)+np.diag(self.Ysh)
        self.G = self.Y.real
        self.B = self.Y.imag
        
        # Matriz Ysh

        
    def saveMatY(self,dec=3):
        aux = np.around(self.Y,decimals=dec)
        np.savetxt("matY.csv", aux, delimiter=",",fmt='%.2f')