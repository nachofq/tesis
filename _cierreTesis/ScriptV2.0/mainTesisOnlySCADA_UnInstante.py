from functions import *
from redElec import *
import sys
plt.close('all')

# =============================================================================
# Preguntas 
# =============================================================================
# 1- Tiene sentido asumir que la función de costo tiene un único mínimo?

# =============================================================================
# ~SENAL IDEAL
# =============================================================================
signal = scipy.io.loadmat('phasors_3phasefault.mat')
signal['bus_v'] = np.delete(signal['bus_v'], (13), axis =0)
signal['bus_v'] = np.delete(signal['bus_v'], (600), axis =1)
signal['baseTiempo'] = np.array([x*1/60 for x in range(600)])

# =============================================================================
# ~ MEDICIONES
# =============================================================================
#### OBSERVABILIDAD
case = case13()
case_falla = case13()
case_falla['bus'][2][5] = case_falla['bus'][2][5] - 1e7
rE       = RedElec(case,pf_max_it=100)
rE_falla = RedElec(case_falla,pf_max_it=100)

ubicacionPMU                    = [10,13]
for flag in ubicacionPMU:
    if flag > rE.cantNodos:
        print("Error. Hay PMUs ubicados en nodos inexistentes")
        sys.exit()
ubicacionPMU.sort()


obs = {}
obs['cantNodos']    = rE.cantNodos
obs['cantRamas']    = rE.cantRamas
obs['nodosO']       = np.unique(ubicacionPMU)
obs['nodosU']       = np.array([x for x in range(1,rE.cantNodos+1) if x not in ubicacionPMU])
obs['ramasF_Obs']   = np.array([rama['ramaN'] for rama in rE.RamaLst if rama['from'] in obs['nodosO']])                    
obs['ramasF_NoObs'] = np.array([rama['ramaN'] for rama in rE.RamaLst if rama['from'] in obs['nodosU']])
obs['ramasT_Obs']   = np.array([rama['ramaN'] for rama in rE.RamaLst if rama['to'] in obs['nodosO']])
obs['ramasT_NoObs'] = np.array([rama['ramaN'] for rama in rE.RamaLst if rama['to'] in obs['nodosU']])

#### PARAMETROS DE SENSORES
intervaloPMU   = signal['baseTiempo'][1]   # en milisegundos
intervaloSCADA = 125*intervaloPMU # en milisegundos 
TVEPMU         = 1#1
TVESCADA       = 5#5

mediciones  = genMed(obs,rE,rE_falla,signal,TVEPMU,TVESCADA,intervaloPMU,intervaloSCADA)
meds        = genMed(obs,rE,rE_falla,signal,1,1,intervaloPMU,intervaloPMU)
Ynew        = Yreord(obs['nodosO'],obs['nodosU'],rE.Y)


# =============================================================================
# LOOP WLS con SCADA ONLY
# =============================================================================
soportePLOT = 1
estadosWLSSCADA = np.ones((rE.cantNodos,mediciones['cantMuestras']))*(1+1j*0.1)

medsSCADA = np.append(mediciones['SCADA_Pinj'],mediciones['SCADA_Pinj'],axis=0)
medsSCADA = np.append(medsSCADA,mediciones['SCADA_PF'],axis=0)
medsSCADA = np.append(medsSCADA,mediciones['SCADA_PT'],axis=0)
medsSCADA = np.append(medsSCADA,mediciones['SCADA_QF'],axis=0)
medsSCADA = np.append(medsSCADA,mediciones['SCADA_QT'],axis=0)
medsSCADA = np.append(medsSCADA,mediciones['SCADA_IFMag'],axis=0)
medsSCADA = np.append(medsSCADA,mediciones['SCADA_ITMag'],axis=0)
medsSCADA = np.append(medsSCADA,mediciones['SCADA_VMag'],axis=0)

instantesSCADA = [x for x in range(600) if x%np.int(intervaloSCADA/intervaloPMU)==0] + [600]
estadosEstimadosSCADA = np.ones((13,len(instantesSCADA)),dtype=complex)
i = 0
for instante in instantesSCADA[0:-1]:
    print(instante)
    refFaseNodo = np.angle(signal['bus_v'][0,instante])
    Vactual = {}
    Vactual['V'] = estadosWLSSCADA[:,instante].copy()
    Vactual['Mag'] = mediciones['SCADA_VMag'][:,instante]
    Vactual['Phase'] = np.angle(estadosWLSSCADA[:,instante]).copy()
    
    xk = [a for a in Vactual['Phase']][1:]
    xk = xk + [a for a in Vactual['Mag']]  
    [H,hx,W] = calcularHyhxOnlySCADA(Vactual,obs['nodosO'],[],[],rE)

    iteracionWLS=0 
    totalIteracionesWLS = 200
    pasoWLS = 0.1    
    while(iteracionWLS < totalIteracionesWLS):
        H = H[:,1:]
        HT = np.transpose(H)
        Dif_Med_hx = medsSCADA[:,instante] - hx                
        G = np.matmul(W,H).real
        G = np.matmul(HT,G).real       
        g = np.matmul(-HT,W).real        
        g = (g.dot(Dif_Med_hx)).real 
        xkp1 = (xk - pasoWLS*np.linalg.inv(G).dot(g)   ).tolist()
        
        Vactual['Phase'] = np.array([refFaseNodo] + xkp1[:rE.cantNodos-1])    
        Vactual['Mag'] = np.array(xkp1[rE.cantNodos-1:])
        Vactual['V'] = np.array(Vactual['Mag'])*(np.cos(Vactual['Phase'])+1j*(np.sin(Vactual['Phase'])))
        
        [H,hx,W]=calcularHyhxOnlySCADA(Vactual,obs['nodosO'],[],[],rE)                   
        xk = xkp1
        iteracionWLS = iteracionWLS +1
        
    estadosEstimadosSCADA[:,i] = Vactual['V']
    i = i +1


for index in range(len(instantesSCADA)-2):
    inicial = instantesSCADA[index]
    final = instantesSCADA[index+1]-1
    
    aux = np.reshape(estadosEstimadosSCADA[:,index],(13,1))
    estadosWLSSCADA[:,inicial:final+1] = np.repeat(aux,125,axis=1) 
#
#
#
###PLOT COMPARATIVA IDEAL
#plt.figure()
#plt.suptitle('Mag(V) Con WLS vs Ideal')  
#ii = 0 
#tiempo = mediciones['tiempo']
#for i in range(obs['cantNodos']):     
#    plt.subplot(3,5,i+1)
#    plt.plot(tiempo[:soportePLOT],np.abs(signal['bus_v'][i,:soportePLOT]))
#    plt.plot(tiempo[:soportePLOT],np.abs(estadosWLSSCADA[i,:soportePLOT]),'kx',markersize=3)
#    plt.gca().set_title("Nodo: " + str(i+1))
#    plt.grid()   
#
#plt.figure()
#plt.suptitle('Phase(V) Con WLS vs Ideal')  
#ii = 0 
#tiempo = mediciones['tiempo']
#for i in range(obs['cantNodos']):     
#    plt.subplot(3,5,i+1)
#    plt.plot(tiempo[:soportePLOT],np.angle(signal['bus_v'][i,:soportePLOT]))
#    plt.plot(tiempo[:soportePLOT],np.angle(estadosWLSSCADA[i,:soportePLOT]),'kx',markersize=3)    
#    plt.gca().set_title("Nodo: " + str(i+1))
#    plt.grid()   
    
# =============================================================================
# LOOP SIN WLS A PARTIR DE TOPOLOGIA Y MEDICIONES
# =============================================================================
#estadosSinWLS   = np.zeros(obs['cantNodos'])
#for j in range(mediciones['cantMuestras']):
#    Pi          = mediciones['SCADA_Pinj'][:,j]
#    Qi          = mediciones['SCADA_Qinj'][:,j]
#    Vi          = mediciones['SCADA_VMag'][:,j]
#    [A,YT,YU]   = calculoMatA(obs,Pi,Qi,Vi,Ynew)

#    Ynew_falla = Yreord(obs['nodosO'],obs['nodosU'],rE_falla.Y)
#    if j>5 and j<10:
#        [A,YT,YU]   = calculoMatA(obs,Pi,Qi,Vi,Ynew_falla)
#    else:
#        [A,YT,YU]   = calculoMatA(obs,Pi,Qi,Vi,Ynew)
##########  NOTA IMPORTANTE#########
#  Tener en cuenta que Ynew carga la topología de red inicial. Por eso al correr                   
# este loop en condiciones ideales (TVE=0 y frecSCADA = frecPMU) el resultado no es ideal
# la info de Ynew carece de la información de la falla.
#    EO   = mediciones['PMU_V'][:,j]   
#    EU   = np.matmul(A,EO)
#     
#    estado_actual = ordenarVectorEstados(EO, EU, obs['nodosO'], obs['nodosU'])
#    estadosSinWLS = np.column_stack([estadosSinWLS,estado_actual['V']])       
#    estado_anterior = estado_actual.copy() 
#estadosSinWLS = estadosSinWLS[:,1:]

#PLOT COMPARATIVA IDEAL
#plt.figure()
#plt.suptitle('Mag(V) Sin WLS vs Ideal')  
#ii = 0 
#tiempo = mediciones['tiempo']
#for i in range(obs['cantNodos']):     
#    plt.subplot(3,5,i+1)
#    plt.plot(tiempo[:soportePLOT],np.abs(signal['bus_v'][i,:soportePLOT]))
#    plt.plot(tiempo[:soportePLOT],np.abs(estadosSinWLS[i,:soportePLOT]),'mx',markersize=3)
#    plt.gca().set_title("Nodo: " + str(i+1))
#    plt.grid()   
#
#plt.figure()
#plt.suptitle('Phase(V) Sin WLS vs Ideal')  
#ii = 0 
#tiempo = mediciones['tiempo']
#for i in range(obs['cantNodos']):     
#    plt.subplot(3,5,i+1)
#    plt.plot(tiempo[:soportePLOT],np.angle(signal['bus_v'][i,:soportePLOT]))
#    plt.plot(tiempo[:soportePLOT],np.angle(estadosSinWLS[i,:soportePLOT]),'mx',markersize=3)
#    plt.gca().set_title("Nodo: " + str(i+1))
#    plt.grid()   


# =============================================================================
# Correccion matriz A CONTINUAR
# =============================================================================

# En la iteración 6 la variacion de potencia supera el umbral por este motivo
# es necesario corregir la matriz A utilizando un bucle WLS.
    
#instanteFalla = 6 
#pasoWLS = 0.1
#cotaVariacion = 0.1
#
#estadoInicial = estadosSinWLS[:,instanteFalla] 
#estado = estadoInicial
#refFaseNodo = np.angle(signal['bus_v'][0,instanteFalla])
#
#
#Vactual  = {}
#Vactual['Mag'] = np.abs(estadoInicial)
#Vactual['Phase'] = np.angle(estadoInicial)
#Vactual['V'] = estadoInicial
#
#
#xk = [a for a in Vactual['Phase']][1:]
#xk = xk + [a for a in Vactual['Mag']]
# 
#condicion = 1
#j = 0
#while (condicion):
#        
#    [H,hx,W]= calcularHyhxSCADA_PMU_Weighted(Vactual,obs['nodosO'],[3],[],rE)
#    H = H[:,1:]
#    HT = np.transpose(H)
#    Dif_Med_hx = mediciones['Vec'][:,instanteFalla] - hx      
#    G = np.matmul(W,H).real
#    G = np.matmul(HT,G).real       
#    g = np.matmul(-HT,W).real        
#    g = (g.dot(Dif_Med_hx)).real 
#    xkp1 = (xk - pasoWLS*np.linalg.inv(G).dot(g)   ).tolist()
#    
#    Vactual['Phase'] = np.array([refFaseNodo] + xkp1[:rE.cantNodos-1])    
#    Vactual['Mag'] = np.array(xkp1[rE.cantNodos-1:])
#    Vactual['V'] = np.array(Vactual['Mag'])*(np.cos(Vactual['Phase'])+1j*(np.sin(Vactual['Phase'])))  
#    estados = p.column_stack(estados,Vactual['V'])               
#    j = j +1   
#
#    variacionEstados = np.abs(estados[:,j]-estados[:,j-1])/np.abs(estados[:,j-1])
#    if np.sum(variacionEstados>)
#        



#
## =============================================================================
# LOOP COMPLETO
# =============================================================================
#soportePLOT = 50
#estadosWLS = estadosSinWLS.copy()  
#for instante in range(soportePLOT):
#    print(instante)
#    refFaseNodo = np.angle(signal['bus_v'][0,instante])
#    Vactual = {}
#    Vactual['V'] = estadosWLS[:,instante].copy()
#    Vactual['Mag'] = np.abs(estadosWLS[:,instante]).copy()
#    Vactual['Phase'] = np.angle(estadosWLS[:,instante]).copy()
#    
#    xk = [a for a in Vactual['Phase']][1:]
#    xk = xk + [a for a in Vactual['Mag']]  
#    [H,hx,W] = calcularHyhxSCADA_PMU_Weighted(Vactual,obs['nodosO'],[],[],rE)
#
#    if instante>5:
#        iteracionWLS=0 
#        totalIteracionesWLS = 150
#        pasoWLS = 0.1    
#        while(iteracionWLS < totalIteracionesWLS):
#            H = H[:,1:]
#            HT = np.transpose(H)
#            Dif_Med_hx = meds['Vec'][:,instante] - hx                
#            G = np.matmul(W,H).real
#            G = np.matmul(HT,G).real       
#            g = np.matmul(-HT,W).real        
#            g = (g.dot(Dif_Med_hx)).real 
#            xkp1 = (xk - pasoWLS*np.linalg.inv(G).dot(g)   ).tolist()
#            
#            Vactual['Phase'] = np.array([refFaseNodo] + xkp1[:rE.cantNodos-1])    
#            Vactual['Mag'] = np.array(xkp1[rE.cantNodos-1:])
#            Vactual['V'] = np.array(Vactual['Mag'])*(np.cos(Vactual['Phase'])+1j*(np.sin(Vactual['Phase'])))
#            
#            [H,hx,W]=calcularHyhxSCADA_PMU_Weighted(Vactual,obs['nodosO'],[],[],rE)                   
#            xk = xkp1
#            iteracionWLS = iteracionWLS +1
#
#    for nodoN in obs['nodosU']:
#        estadosWLS[nodoN-1,instante] = Vactual['V'][nodoN-1]
##
###PLOT COMPARATIVA IDEAL
#plt.figure()
#plt.suptitle('Mag(V) Con WLS vs Ideal')  
#ii = 0 
#tiempo = mediciones['tiempo']
#for i in range(obs['cantNodos']):     
#    plt.subplot(3,5,i+1)
#    plt.plot(tiempo[:soportePLOT],np.abs(signal['bus_v'][i,:soportePLOT]))
#    plt.plot(tiempo[:soportePLOT],np.abs(estadosSinWLS[i,:soportePLOT]),'mx',markersize=3)
#    plt.plot(tiempo[:soportePLOT],np.abs(estadosWLS[i,:soportePLOT]),'kx',markersize=3)
#    plt.gca().set_title("Nodo: " + str(i+1))
#    plt.grid()   
#
#plt.figure()
#plt.suptitle('Phase(V) Con WLS vs Ideal')  
#ii = 0 
#tiempo = mediciones['tiempo']
#for i in range(obs['cantNodos']):     
#    plt.subplot(3,5,i+1)
#    plt.plot(tiempo[:soportePLOT],np.angle(signal['bus_v'][i,:soportePLOT]))
#    plt.plot(tiempo[:soportePLOT],np.angle(estadosSinWLS[i,:soportePLOT]),'mx',markersize=3)
#    plt.plot(tiempo[:soportePLOT],np.angle(estadosWLS[i,:soportePLOT]),'kx',markersize=3)    
#    plt.gca().set_title("Nodo: " + str(i+1))
#    plt.grid()   
#        
    
# =============================================================================
# VALIDACION DE BUCLE WLS
# =============================================================================
# Esta validación sirve para mostrar que, cuando el estado inicial del loop WLS
# es el provisto por el loop lineal a partir de la variable "mediciones"
# (TVESCADA y TVEPMU != 0) y las mediciones utilizadas para el loop WLS cargadas
# en la variable mediciones2 no poseen ruido, la convergencia del loop WLS es 
# perfecta.

# Es interesante notar lo que ocurre durante la falla. El algoritmo no converge.
# la única explicación que encuentro es que mi vector de mediciones sin ruido
# está errado durante la falla. Esto puede tener que ver con que no estoy 
# considerando el cambio en la topología de red para calcular las mediciones.
#
#instante = 6
#
#intervaloPMU2 = intervaloPMU
#intervaloSCADA2 = intervaloPMU
#mediciones2 = genMed(obs,rE,rE_falla,signal,0,0,intervaloPMU2,intervaloSCADA2)
#mediciones = mediciones
#refFaseNodo = np.angle(signal['bus_v'][0,instante])
#
#Videal          = {}
#Videal['V']     = signal['bus_v'][:,instante]
#Videal['Mag']   = np.abs(Videal['V'])
#Videal['Phase'] = np.angle(Videal['V'])
#
#Vactual = {}
#Vactual['V'] = estadosSinWLS[:,instante].copy()
#Vactual['Mag'] = np.abs(estadosSinWLS[:,instante]).copy()
#Vactual['Phase'] = np.angle(estadosSinWLS[:,instante]).copy()
#
#
#estados={}                                  
#estados['Mag']   = Vactual['Mag'].copy()
#estados['Phase'] = Vactual['Phase'].copy()
#
#xk = [a for a in Vactual['Phase']][1:]
#xk = xk + [a for a in Vactual['Mag']]  
#[H,hx,W] = calcularHyhxSCADA_PMU_Weighted(Vactual,obs['nodosO'],[3,4],[],rE)
#
#
#iteracionWLS=0 
#totalIteracionesWLS = 100
#pasoWLS = 0.1    
#while(iteracionWLS < totalIteracionesWLS):
#    H = H[:,1:]
#    HT = np.transpose(H)
#    Dif_Med_hx = mediciones['Vec'][:,instante] - hx                
#    G = np.matmul(W,H).real
#    G = np.matmul(HT,G).real       
#    g = np.matmul(-HT,W).real        
#    g = (g.dot(Dif_Med_hx)).real 
#    xkp1 = (xk - pasoWLS*np.linalg.inv(G).dot(g)   ).tolist()
#    
#    Vactual['Phase'] = np.array([refFaseNodo] + xkp1[:rE.cantNodos-1])    
#    Vactual['Mag'] = np.array(xkp1[rE.cantNodos-1:])
#    Vactual['V'] = np.array(Vactual['Mag'])*(np.cos(Vactual['Phase'])+1j*(np.sin(Vactual['Phase'])))
#    
#    [H,hx,W]=calcularHyhxSCADA_PMU_Weighted(Vactual,obs['nodosO'],[3,4],[],rE)                   
#    xk = xkp1
#    iteracionWLS = iteracionWLS +1
#    
#    estados['Mag'] = np.column_stack([estados['Mag'],Vactual['Mag']])
#    estados['Phase'] = np.column_stack([estados['Phase'],Vactual['Phase']])
#    
#idealMag = np.reshape(Videal['Mag'],(13,1))*np.ones((1,iteracionWLS))
#idealPhase = np.reshape(Videal['Phase'],(13,1))*np.ones((1,iteracionWLS))
#
#plt.figure()    
#for i in range(idealMag.shape[0]):
#    plt.subplot(2,7,i+1)
#    plt.plot(idealMag[i,:])
#    plt.plot(estados['Mag'][i,:],markersize=1)
#    plt.gca().set_title("Nodo: " + str(i+1))
#    plt.grid()    
#
#plt.figure()    
#for i in range(idealMag.shape[0]):
#    plt.subplot(2,7,i+1)
#    plt.plot(idealPhase[i,:]*180/pi)
#    plt.plot(estados['Phase'][i,:]*180/pi,markersize=1)
#    plt.gca().set_title("Nodo: " + str(i+1))
#    plt.grid()    
#    
# 
#TVEMag  =  np.zeros(estados['Mag'].shape)
#for i in range(estados['Mag'].shape[1]):
#    TVEMag[:,i] = (np.abs(Videal['Mag'] - estados['Mag'][:,i]))*100/np.abs(np.array(Videal['Mag']))
#
#
#plt.figure()    
#for i in range(TVEMag.shape[0]):
#    plt.subplot(2,7,i+1)
#    plt.plot(TVEMag[i,:])
##    plt.gca().set_ylim([0, 25])
#    plt.gca().set_title("Nodo: " + str(i+1))
#    plt.grid()    
#
#    
#TVEPhase   =  np.zeros(estados['Phase'].shape)
#for i in range(estados['Phase'].shape[1]):
#    TVEPhase[:,i] = (np.abs(Videal['Phase']*180/pi -  estados['Phase'][:,i]*180/pi))*100/np.abs(Videal['Phase']*180/pi)
#
#plt.figure()    
#for i in range(TVEPhase.shape[0]):
#    plt.subplot(2,7,i+1)
#    plt.plot(TVEPhase[i,:])
##    plt.gca().set_ylim([0, 25])
#    plt.gca().set_title("Nodo: " + str(i+1))
#    plt.grid()   
   
    
##########################################
i = 6
soportePLOT = 50
plt.figure()
plt.plot(tiempo[:soportePLOT],np.abs(signal['bus_v'][i,:soportePLOT]))
plt.plot(tiempo[:soportePLOT],np.abs(estadosWLSSCADA[i,:soportePLOT]),'k.',markersize=1)
plt.plot(tiempo[:soportePLOT],np.abs(estadosSinWLS[i,:soportePLOT]),'m.',markersize=1)
plt.plot(tiempo[:soportePLOT],np.abs(estadosWLS[i,:soportePLOT]),'gx',markersize=1)
plt.gca().set_title("Magnitud de Tensión - Nodo 7")
plt.legend(["Ideal","Estimación WLS SCADA Only","Estimación Lineal EU = HEO","Estimación Lineal+WLS"])
plt.grid()
plt.savefig("Magnitud_50.pdf")

plt.figure()
plt.plot(tiempo[:soportePLOT],np.angle(signal['bus_v'][i,:soportePLOT]))
plt.plot(tiempo[:soportePLOT],np.angle(estadosWLSSCADA[i,:soportePLOT]),'k.',markersize=1)
plt.plot(tiempo[:soportePLOT],np.angle(estadosSinWLS[i,:soportePLOT]),'m.',markersize=1)
plt.plot(tiempo[:soportePLOT],np.angle(estadosWLS[i,:soportePLOT]),'gx',markersize=1)  
plt.gca().set_title("Fase de Tensión - Nodo 7")
plt.legend(["Ideal","Estimación WLS SCADA Only","Estimación Lineal EU = HEO","Estimación Lineal+WLS"])
plt.grid()
plt.savefig("Fase_50.pdf")