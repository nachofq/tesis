from common import *

def calcularHyhxSCADA_PMU_Weighted(Vestados,nodosO,nodosUZona1,nodosUZona2,redElec):
# El vector de estados inlcuye el valor de referencia para las fases.
    
    pesosZona1 = 0.1
    pesosZona2 = 1 
    pesosSCADA = 5
    pesosPMU   = 20
# Observabilidad de Nodos y Ramas    
    nodos = [x for x in range(1,redElec.cantNodos+1)] # SCADA puede ver todos los nodos
    nodosU = np.array([x for x in nodos if x not in nodosO ])
    ramasOF = []
    ramasOT = []
    for rama in redElec.RamaLst:
        if rama['from'] in nodosO:
            ramasOF.append(rama['ramaN'])
        if rama['to'] in nodosO:
            ramasOT.append(rama['ramaN']) 


#######################################################################
############################### SCADA #################################
#######################################################################

##############################  
#### HPINJ & HQINJ & HVMag####
############################## 
        
    # Inicializacion H_SCADA
    HPinj = {}              
    HPinj['dPhase'] = np.zeros((len(nodos),redElec.cantNodos),dtype=np.complex_)
    HPinj['dMag'] = np.zeros((len(nodos),redElec.cantNodos),dtype=np.complex_)
    HQinj = {}              
    HQinj['dPhase'] = np.zeros((len(nodos),redElec.cantNodos),dtype=np.complex_)
    HQinj['dMag'] = np.zeros((len(nodos),redElec.cantNodos),dtype=np.complex_)
    HVMag = {}
    HVMag['dPhase'] = np.zeros((redElec.cantNodos,redElec.cantNodos),dtype=np.complex_)
    HVMag['dMag'] = np.zeros((redElec.cantNodos,redElec.cantNodos),dtype=np.complex_)
    # Inicializacion h_SCADA
    hxPinj = np.zeros((len(nodos),1),dtype=np.complex_)
    hxQinj = np.zeros((len(nodos),1),dtype=np.complex_)
    hxVMag = np.zeros((len(nodos),1),dtype=np.complex_)
    # Inicializacion WSCADA
    W_SCADA_Pinj = np.zeros(redElec.cantNodos)
    W_SCADA_Qinj = np.zeros(redElec.cantNodos)
    W_SCADA_VMag = np.zeros(redElec.cantNodos)
        
    
    for i in [x-1 for x in nodos]:      
        vecinos = np.unique(redElec.NodoLst[i]['Vecinos']).tolist()
        fases = [(Vestados['Phase'][i] - Vestados['Phase'][j-1]) for j in nodos]
        V_Mag_i = Vestados['Mag'][i]
        V_Mag_vecinos = [Vestados['Mag'][ii-1] for ii in vecinos]
        G_i = redElec.G[i] 
        B_i = redElec.B[i]
        
        ## Matriz H
        # HPinj & HQinj
        HP_V_Mag_vecinos_Phasefactor = [(-G_i[ii-1]*sin(fases[ii-1]) + B_i[ii-1]*cos(fases[ii-1])) for ii in vecinos]
        HP_V_Mag_vecinos_Magfactor = [(G_i[ii-1]*cos(fases[ii-1]) + B_i[ii-1]*sin(fases[ii-1])) for ii in vecinos]
        HQ_V_Mag_vecinos_Phasefactor = [(G_i[ii-1]*cos(fases[ii-1]) + B_i[ii-1]*sin(fases[ii-1])) for ii in vecinos]
        HQ_V_Mag_vecinos_Magfactor = [(G_i[ii-1]*sin(fases[ii-1]) - B_i[ii-1]*cos(fases[ii-1])) for ii in vecinos]
        
        HPinj['dPhase'][i][i] =  V_Mag_i*sum([V_Mag_vecinos[k]*HP_V_Mag_vecinos_Phasefactor[k] for k in range(len(V_Mag_vecinos))])  
        HPinj['dMag'][i][i] = sum([V_Mag_vecinos[k]*HP_V_Mag_vecinos_Magfactor[k] for k in range(len(V_Mag_vecinos))]) + 2*G_i[i]*V_Mag_i 
        HQinj['dPhase'][i][i] =  V_Mag_i*sum([V_Mag_vecinos[k]*HQ_V_Mag_vecinos_Phasefactor[k] for k in range(len(V_Mag_vecinos))])
        HQinj['dMag'][i][i] = sum([V_Mag_vecinos[k]*HQ_V_Mag_vecinos_Magfactor[k] for k in range(len(V_Mag_vecinos))]) -2*B_i[i]*V_Mag_i 

        for j in [x-1 for x in vecinos]:
             HPinj['dPhase'][i][j] = V_Mag_i*Vestados['Mag'][j]*(G_i[j]*sin(fases[j])-B_i[j]*cos(fases[j]))
             HPinj['dMag'][i][j] = V_Mag_i*(G_i[j]*cos(fases[j])+B_i[j]*sin(fases[j]))  
             HQinj['dPhase'][i][j] = V_Mag_i*Vestados['Mag'][j]*(-G_i[j]*cos(fases[j])-B_i[j]*sin(fases[j]))
             HQinj['dMag'][i][j] = V_Mag_i*(G_i[j]*sin(fases[j])-B_i[j]*cos(fases[j]))             
        
        if i+1 in nodosUZona1:
            W_SCADA_Pinj[i] = pesosZona1
            W_SCADA_Qinj[i] = pesosZona1
            W_SCADA_VMag[i] = pesosZona1
        elif i+1 in nodosUZona2:
            W_SCADA_Pinj[i] = pesosZona2
            W_SCADA_Qinj[i] = pesosZona2
            W_SCADA_VMag[i] = pesosZona2
        else:
            W_SCADA_Pinj[i] = 1
            W_SCADA_Qinj[i] = 1
            W_SCADA_VMag[i] = 1
        
        
        # HVMag
        HVMag['dMag'][i][i]=1

        # Vector h(x)
     
        hxPinj_factor = [(G_i[ii-1]*cos(fases[ii-1]) + B_i[ii-1]*sin(fases[ii-1])) for ii in vecinos]
        hxPinj_self   = V_Mag_i**2*redElec.G[i][i]
        hxPinj[i]     = V_Mag_i*sum([V_Mag_vecinos[k]*hxPinj_factor[k] for k in range(len(V_Mag_vecinos))])
        hxPinj[i]     = hxPinj[i] + hxPinj_self 
        
        hxQinj_factor = [(G_i[ii-1]*sin(fases[ii-1]) - B_i[ii-1]*cos(fases[ii-1])) for ii in vecinos]
        hxQinj_self = -V_Mag_i**2*redElec.B[i][i]
        hxQinj[i] = V_Mag_i*sum([V_Mag_vecinos[k]*hxQinj_factor[k] for k in range(len(V_Mag_vecinos))])         
        hxQinj[i] = hxQinj[i] + hxQinj_self 
        
        hxVMag[i] = V_Mag_i
        
    # Append
    HPinj['Mat'] = np.append(HPinj['dPhase'],HPinj['dMag'],axis=1)        
    HQinj['Mat'] = np.append(HQinj['dPhase'],HQinj['dMag'],axis=1)
    HVMag['Mat'] = np.append(HVMag['dPhase'],HVMag['dMag'],axis=1)        
    
    
################################  
#### HPflow & HQflow & HIMag####
################################       
    
    HPflowF = {}
    HPflowF['dPhase'] = np.zeros((redElec.cantRamas,redElec.cantNodos),dtype=np.complex_)
    HPflowF['dMag'] = np.zeros((redElec.cantRamas,redElec.cantNodos),dtype=np.complex_)
    HPflowF['Mat'] = []
    HPflowT = {}
    HPflowT['dPhase'] = np.zeros((redElec.cantRamas,redElec.cantNodos),dtype=np.complex_)
    HPflowT['dMag'] = np.zeros((redElec.cantRamas,redElec.cantNodos),dtype=np.complex_)
    HPflowT['Mat'] = []
    HQflowF = {}
    HQflowF['dPhase'] = np.zeros((redElec.cantRamas,redElec.cantNodos),dtype=np.complex_)
    HQflowF['dMag'] = np.zeros((redElec.cantRamas,redElec.cantNodos),dtype=np.complex_)
    HQflowF['Mat'] = []
    HQflowT = {}
    HQflowT['dPhase'] = np.zeros((redElec.cantRamas,redElec.cantNodos),dtype=np.complex_)
    HQflowT['dMag'] = np.zeros((redElec.cantRamas,redElec.cantNodos),dtype=np.complex_)
    HQflowT['Mat'] = []
    
    HIMagF = {}
    HIMagF['dPhase'] = np.zeros((redElec.cantRamas,redElec.cantNodos),dtype=np.complex_)
    HIMagF['dMag'] = np.zeros((redElec.cantRamas,redElec.cantNodos),dtype=np.complex_)
    HIMagF['Mat'] = []
    HIMagT = {}
    HIMagT['dPhase'] = np.zeros((redElec.cantRamas,redElec.cantNodos),dtype=np.complex_)
    HIMagT['dMag'] = np.zeros((redElec.cantRamas,redElec.cantNodos),dtype=np.complex_)
    HIMagT['Mat'] = []
          
    hxPflowF = np.zeros((redElec.cantRamas,1),dtype=np.complex_)        
    hxPflowT = np.zeros((redElec.cantRamas,1),dtype=np.complex_)        
    hxQflowF = np.zeros((redElec.cantRamas,1),dtype=np.complex_)
    hxQflowT = np.zeros((redElec.cantRamas,1),dtype=np.complex_)
    hxIMagF = np.zeros((redElec.cantRamas,1),dtype=np.complex_) 
    hxIMagT = np.zeros((redElec.cantRamas,1),dtype=np.complex_)
    
    W_SCADA_PflowF = np.zeros(redElec.cantRamas)
    W_SCADA_PflowT = np.zeros(redElec.cantRamas)
    W_SCADA_QflowF = np.zeros(redElec.cantRamas)
    W_SCADA_QflowT = np.zeros(redElec.cantRamas)
    W_SCADA_IMagF = np.zeros(redElec.cantRamas)
    W_SCADA_IMagT = np.zeros(redElec.cantRamas)
    
    for rama in redElec.RamaLst:
        fr = rama['from']
        to = rama['to']
        Vfr = Vestados['Mag'][fr - 1]
        Vfr_phase = Vestados['Phase'][fr - 1]
        Vto = Vestados['Mag'][to - 1]
        Vto_phase = Vestados['Phase'][to - 1]
        tap = rama['TapMod']
        I_fr_to = np.abs(rama['IF'])
        I_to_fr = np.abs(rama['IT'])
        b = np.imag(rama['ys'])
        g = np.real(rama['ys'])
        bsfr = np.real(rama['B'])/2# + np.real(redElec.NodoLst[fr-1]['bshunt'])
        bsto = np.real(rama['B'])/2#+ np.real(redElec.NodoLst[to-1]['bshunt'])
        phi_fr_to = Vestados['Phase'][fr - 1] - Vestados['Phase'][to - 1]
        phi_to_fr = - phi_fr_to

        
        # Matriz H de Pflow y Qflow
        HPflowF['dPhase'][rama['ramaN']-1][fr-1] = (Vfr/tap)*Vto*(g*sin(phi_fr_to) - b*cos(phi_fr_to))
        HPflowF['dPhase'][rama['ramaN']-1][to-1] = -(Vfr/tap)*Vto*(g*sin(phi_fr_to) - b*cos(phi_fr_to))
        HPflowF['dMag'][rama['ramaN']-1][fr-1] = -(Vto/tap)*(g*cos(phi_fr_to) + b*sin(phi_fr_to)) + 2*(g)*(Vfr/(tap**2)) 
        HPflowF['dMag'][rama['ramaN']-1][to-1] = -(Vfr/tap)*(g*cos(phi_fr_to) + b*sin(phi_fr_to)) 
        
        HPflowT['dPhase'][rama['ramaN']-1][fr-1] = -(Vfr/tap)*Vto*(g*sin(phi_to_fr) - b*cos(phi_to_fr))
        HPflowT['dPhase'][rama['ramaN']-1][to-1] = -(Vfr/tap)*Vto*(-g*sin(phi_to_fr) + b*cos(phi_to_fr))
        HPflowT['dMag'][rama['ramaN']-1][fr-1] =   -(Vto/tap)*(g*cos(phi_to_fr) + b*sin(phi_to_fr))
        HPflowT['dMag'][rama['ramaN']-1][to-1] = 2*(Vto)*(g) -(Vfr/tap)*(g*cos(phi_to_fr) + b*sin(phi_to_fr))            
                    
        HQflowF['dPhase'][rama['ramaN']-1][fr-1] = -(Vfr/tap)*Vto*(g*cos(phi_fr_to) + b*sin(phi_fr_to))
        HQflowF['dPhase'][rama['ramaN']-1][to-1] = (Vfr/tap)*Vto*(g*cos(phi_fr_to) + b*sin(phi_fr_to))
        HQflowF['dMag'][rama['ramaN']-1][fr-1] = -(Vto/tap)*(g*sin(phi_fr_to) - b*cos(phi_fr_to)) - 2*(b+bsfr)*(Vfr/(tap**2))
        HQflowF['dMag'][rama['ramaN']-1][to-1] = -(Vfr/tap)*(g*sin(phi_fr_to) - b*cos(phi_fr_to))
        
        HQflowT['dPhase'][rama['ramaN']-1][fr-1] = (Vfr/tap)*Vto*(g*cos(phi_to_fr) + b*sin(phi_to_fr))
        HQflowT['dPhase'][rama['ramaN']-1][to-1] = -(Vfr/tap)*Vto*(g*cos(phi_to_fr) + b*sin(phi_to_fr))
        HQflowT['dMag'][rama['ramaN']-1][fr-1] = -(Vto/tap)*(g*sin(phi_to_fr) - b*cos(phi_to_fr))
        HQflowT['dMag'][rama['ramaN']-1][to-1] = -2*(Vto)*(bsto+b)-(Vfr/tap)*(g*sin(phi_to_fr) - b*cos(phi_to_fr))            
        
        # Vector h(x) de Pflow y Qflow           
                    
        hxPflowF[rama['ramaN']-1] = ((Vfr/tap)**2)*(g) - (Vfr/tap)*Vto*(g*cos(phi_fr_to) + b*sin(phi_fr_to))
        hxPflowT[rama['ramaN']-1] = (Vto**2)*(g) - Vto*(Vfr/tap)*(g*cos(phi_to_fr)+b*sin(phi_to_fr))
                 
        hxQflowF[rama['ramaN']-1] = -((Vfr/tap)**2)*(bsfr+b) - (Vfr/tap)*Vto*(g*sin(phi_fr_to) - b*cos(phi_fr_to))       
        hxQflowT[rama['ramaN']-1] = -((Vto)**2)*(bsto+b) - Vto*(Vfr/tap)*(g*sin(phi_to_fr) - b*cos(phi_to_fr))


        # Matriz H de Imag

        Pft = hxPflowF[rama['ramaN']-1]
        Qft = hxQflowF[rama['ramaN']-1]
        dPft_dTf = HPflowF['dPhase'][rama['ramaN']-1][fr-1]
        dPft_dTt = HPflowF['dPhase'][rama['ramaN']-1][to-1]
        dPft_dVf = HPflowF['dMag'][rama['ramaN']-1][fr-1]
        dPft_dVt = HPflowF['dMag'][rama['ramaN']-1][to-1]           
        dQft_dTf = HQflowF['dPhase'][rama['ramaN']-1][fr-1]
        dQft_dTt = HQflowF['dPhase'][rama['ramaN']-1][to-1]
        dQft_dVf = HQflowF['dMag'][rama['ramaN']-1][fr-1]
        dQft_dVt = HQflowF['dMag'][rama['ramaN']-1][to-1]
        
        HIMagF['dPhase'][rama['ramaN']-1][fr-1] = (1/Vfr)*((1/(2*np.sqrt(Pft**2 + Qft**2))) * (2*Pft*dPft_dTf+2*Qft*dQft_dTf) )
        HIMagF['dPhase'][rama['ramaN']-1][to-1] = (1/Vfr)*((1/(2*np.sqrt(Pft**2 + Qft**2))) * (2*Pft*dPft_dTt+2*Qft*dQft_dTt) )
        HIMagF['dMag'][rama['ramaN']-1][fr-1] = ((((1/(2*np.sqrt(Pft**2 + Qft**2))) * (2*Pft*dPft_dVf+2*Qft*dQft_dVf) )*Vfr) - np.sqrt(Pft**2 + Qft**2)) /(Vfr**2)
        HIMagF['dMag'][rama['ramaN']-1][to-1] =  (1/Vfr)*((1/(2*np.sqrt(Pft**2 + Qft**2))) * (2*Pft*dPft_dVt+2*Qft*dQft_dVt) )
        HIMagF['Mat'] = np.append(HIMagF['dPhase'],HIMagF['dMag'],axis=1)

        Ptf = hxPflowT[rama['ramaN']-1]
        Qtf = hxQflowT[rama['ramaN']-1]
        dPtf_dTf = HPflowT['dPhase'][rama['ramaN']-1][fr-1]
        dPtf_dTt = HPflowT['dPhase'][rama['ramaN']-1][to-1]
        dPtf_dVf = HPflowT['dMag'][rama['ramaN']-1][fr-1]
        dPtf_dVt = HPflowT['dMag'][rama['ramaN']-1][to-1]           
        dQtf_dTf = HQflowT['dPhase'][rama['ramaN']-1][fr-1]
        dQtf_dTt = HQflowT['dPhase'][rama['ramaN']-1][to-1]
        dQtf_dVf = HQflowT['dMag'][rama['ramaN']-1][fr-1]
        dQtf_dVt = HQflowT['dMag'][rama['ramaN']-1][to-1]
        
        HIMagT['dPhase'][rama['ramaN']-1][fr-1] = (1/Vto)*((1/(2*np.sqrt(Ptf**2 + Qtf**2))) * (2*Ptf*dPtf_dTf+2*Qtf*dQtf_dTf) )
        HIMagT['dPhase'][rama['ramaN']-1][to-1] = (1/Vto)*((1/(2*np.sqrt(Ptf**2 + Qtf**2))) * (2*Ptf*dPtf_dTt+2*Qtf*dQtf_dTt) )
        HIMagT['dMag'][rama['ramaN']-1][fr-1] = (1/Vto)*((1/(2*np.sqrt(Ptf**2 + Qtf**2))) * (2*Ptf*dPtf_dVt+2*Qtf*dQtf_dVt) )
        HIMagT['dMag'][rama['ramaN']-1][to-1] = ((((1/(2*np.sqrt(Ptf**2 + Qtf**2))) * (2*Ptf*dPtf_dVf+2*Qtf*dQtf_dVf) )*Vto) - np.sqrt(Ptf**2 + Qtf**2)) /(Vto**2)
        HIMagT['Mat'] = np.append(HIMagT['dPhase'],HIMagT['dMag'],axis=1)             

       ########################################
       
        # Vector h(x) de IMag  
        hxIMagF[rama['ramaN']-1] =  ((hxPflowF[rama['ramaN']-1]**2 +  hxQflowF[rama['ramaN']-1]**2)**0.5)/Vfr
        hxIMagT[rama['ramaN']-1] =  ((hxPflowT[rama['ramaN']-1]**2 +  hxQflowT[rama['ramaN']-1]**2)**0.5)/Vto        
        
       ######################################
       # Matriz de pesos  
        if fr in nodosUZona1 or to in nodosUZona1:
            W_SCADA_PflowF[rama['ramaN']-1] = pesosZona1
            W_SCADA_PflowT[rama['ramaN']-1] = pesosZona1 
            W_SCADA_QflowF[rama['ramaN']-1] = pesosZona1
            W_SCADA_QflowT[rama['ramaN']-1] = pesosZona1
            W_SCADA_IMagF[rama['ramaN']-1]  = pesosZona1
            W_SCADA_IMagT[rama['ramaN']-1]  = pesosZona1
        if fr in nodosUZona2 or to in nodosUZona2:
            W_SCADA_PflowF[rama['ramaN']-1] = pesosZona2
            W_SCADA_PflowT[rama['ramaN']-1] = pesosZona2 
            W_SCADA_QflowF[rama['ramaN']-1] = pesosZona2
            W_SCADA_QflowT[rama['ramaN']-1] = pesosZona2
            W_SCADA_IMagF[rama['ramaN']-1]  = pesosZona2
            W_SCADA_IMagT[rama['ramaN']-1]  = pesosZona2
        else:
            W_SCADA_PflowF[rama['ramaN']-1] = pesosSCADA
            W_SCADA_PflowT[rama['ramaN']-1] = pesosSCADA 
            W_SCADA_QflowF[rama['ramaN']-1] = pesosSCADA
            W_SCADA_QflowT[rama['ramaN']-1] = pesosSCADA
            W_SCADA_IMagF[rama['ramaN']-1]  = pesosSCADA
            W_SCADA_IMagT[rama['ramaN']-1]  = pesosSCADA
       
    HPflowF['Mat'] = np.append(HPflowF['dPhase'],HPflowF['dMag'],axis=1)
    HPflowT['Mat'] = np.append(HPflowT['dPhase'],HPflowT['dMag'],axis=1)
    HQflowF['Mat'] = np.append(HQflowF['dPhase'],HQflowF['dMag'],axis=1)
    HQflowT['Mat'] = np.append(HQflowT['dPhase'],HQflowT['dMag'],axis=1)        
    HIMagF['Mat'] = np.append(HIMagF['dPhase'],HIMagF['dMag'],axis=1)
    HIMagT['Mat'] = np.append(HIMagT['dPhase'],HIMagT['dMag'],axis=1)
    
    
   
#######################################################################
###############################  PMU  #################################
#######################################################################
         
    PMU_HVMag = {}              
    PMU_HVMag['dPhase'] = np.zeros((len(nodosO),redElec.cantNodos),dtype=np.complex_)
    PMU_HVMag['dMag'] = np.zeros((len(nodosO),redElec.cantNodos),dtype=np.complex_)
    PMU_HVMag['Mat'] = []    
    PMU_HVPhase = {}              
    PMU_HVPhase['dPhase'] = np.zeros((len(nodosO),redElec.cantNodos),dtype=np.complex_)
    PMU_HVPhase['dMag'] = np.zeros((len(nodosO),redElec.cantNodos),dtype=np.complex_)
    PMU_HVPhase['Mat'] = []    
    PMU_hxVMag = np.zeros((len(nodosO),1),dtype=np.complex_)
    PMU_hxVPhase = np.zeros((len(nodosO),1),dtype=np.complex_)

    filaIndex = 0
    for nodo in redElec.NodoLst:
        nodoActualIndex = nodo['nodoN']
        if nodoActualIndex in nodosO:
            PMU_HVMag['dMag'][filaIndex][nodoActualIndex - 1]       = 1 
            PMU_HVPhase['dPhase'][filaIndex][nodoActualIndex - 1]   = 1
            PMU_hxVMag[filaIndex]   = Vestados['Mag'][nodoActualIndex-1]
            PMU_hxVPhase[filaIndex] = Vestados['Phase'][nodoActualIndex-1]
            filaIndex = filaIndex + 1 
        
    PMU_HVMag['Mat']= np.append(PMU_HVMag['dPhase'], PMU_HVMag['dMag'],axis=1)
    PMU_HVPhase['Mat']= np.append(PMU_HVPhase['dPhase'], PMU_HVPhase['dMag'],axis=1)

    W_PMU_VMag      = pesosPMU*np.ones(len(PMU_hxVMag))
    W_PMU_VPhase    = pesosPMU*np.ones(len(PMU_hxVPhase))


    PMU_HIMagF = {}
    PMU_HIMagF['dPhase'] = np.zeros((len(ramasOF),redElec.cantNodos),dtype=np.complex_)
    PMU_HIMagF['dMag'] = np.zeros((len(ramasOF),redElec.cantNodos),dtype=np.complex_)
    PMU_HIMagF['Mat'] = []
    PMU_HIMagT = {}
    PMU_HIMagT['dPhase'] = np.zeros((len(ramasOT),redElec.cantNodos),dtype=np.complex_)
    PMU_HIMagT['dMag'] = np.zeros((len(ramasOT),redElec.cantNodos),dtype=np.complex_)
    PMU_HIMagT['Mat'] = []
    
    PMU_HIFasF = {}
    PMU_HIFasF['dPhase'] = np.zeros((len(ramasOF),redElec.cantNodos),dtype=np.complex_)
    PMU_HIFasF['dMag'] = np.zeros((len(ramasOF),redElec.cantNodos),dtype=np.complex_)
    PMU_HIFasF['Mat'] = []
    PMU_HIFasT = {}
    PMU_HIFasT['dPhase'] = np.zeros((len(ramasOT),redElec.cantNodos),dtype=np.complex_)
    PMU_HIFasT['dMag'] = np.zeros((len(ramasOT),redElec.cantNodos),dtype=np.complex_)
    PMU_HIFasT['Mat'] = []    

    PMU_hxIMagF = np.zeros((len(ramasOF),1),dtype=np.complex_) 
    PMU_hxIMagT = np.zeros((len(ramasOT),1),dtype=np.complex_)
    PMU_hxIFasF = np.zeros((len(ramasOF),1),dtype=np.complex_) 
    PMU_hxIFasT = np.zeros((len(ramasOT),1),dtype=np.complex_)
   
    filaIndexIF = 0
    filaIndexIT = 0
    for rama in redElec.RamaLst:
        ramaActualIndex = rama['ramaN']        
        fr = rama['from']
        to = rama['to']
        Vfr = Vestados['Mag'][fr - 1]
        Vfr_phase = Vestados['Phase'][fr - 1]
        Vto = Vestados['Mag'][to - 1]
        Vto_phase = Vestados['Phase'][to - 1]
        tap = rama['TapMod']
        I_fr_to = np.abs(rama['IF'])
        I_to_fr = np.abs(rama['IT'])
        b = np.imag(rama['ys'])
        g = np.real(rama['ys'])
        bs = np.real(rama['B'])/2 
        phi_fr_to = Vestados['Phase'][fr - 1] - Vestados['Phase'][to - 1]
        phi_to_fr = - phi_fr_to
        

        if ramaActualIndex in ramasOF:
            # Potencias FROM - TO Para calculo de I         
                     
            Pft = hxPflowF[rama['ramaN']-1]
            Qft = hxQflowF[rama['ramaN']-1]
            dPft_dTf = HPflowF['dPhase'][rama['ramaN']-1][fr-1]
            dPft_dTt = HPflowF['dPhase'][rama['ramaN']-1][to-1]
            dPft_dVf = HPflowF['dMag'][rama['ramaN']-1][fr-1]
            dPft_dVt = HPflowF['dMag'][rama['ramaN']-1][to-1]           
            dQft_dTf = HQflowF['dPhase'][rama['ramaN']-1][fr-1]
            dQft_dTt = HQflowF['dPhase'][rama['ramaN']-1][to-1]
            dQft_dVf = HQflowF['dMag'][rama['ramaN']-1][fr-1]
            dQft_dVt = HQflowF['dMag'][rama['ramaN']-1][to-1]
                
            PMU_HIMagF['dPhase'][filaIndexIF][fr-1] = (1/Vfr)*((1/(2*np.sqrt(Pft**2 + Qft**2))) * (2*Pft*dPft_dTf+2*Qft*dQft_dTf) )
            PMU_HIMagF['dPhase'][filaIndexIF][to-1] = (1/Vfr)*((1/(2*np.sqrt(Pft**2 + Qft**2))) * (2*Pft*dPft_dTt+2*Qft*dQft_dTt) )
            PMU_HIMagF['dMag'][filaIndexIF][fr-1] = ((((1/(2*np.sqrt(Pft**2 + Qft**2))) * (2*Pft*dPft_dVf+2*Qft*dQft_dVf) )*Vfr) - np.sqrt(Pft**2 + Qft**2)) /(Vfr**2)
            PMU_HIMagF['dMag'][filaIndexIF][to-1] =  (1/Vfr)*((1/(2*np.sqrt(Pft**2 + Qft**2))) * (2*Pft*dPft_dVt+2*Qft*dQft_dVt) )
            PMU_HIMagF['Mat'] = np.append(PMU_HIMagF['dPhase'],PMU_HIMagF['dMag'],axis=1)
            
            PMU_HIFasF['dPhase'][filaIndexIF][fr-1] = (1/(1+(Qft/Pft)**2))*((-dQft_dTf*Pft + Qft*dPft_dTf)/(Pft**2))+1 
            PMU_HIFasF['dPhase'][filaIndexIF][to-1] = (1/(1+(Qft/Pft)**2))*((-dQft_dTt*Pft + Qft*dPft_dTt)/(Pft**2))
            PMU_HIFasF['dMag'][filaIndexIF][fr-1] = (1/(1+(Qft/Pft)**2))*((-dQft_dVf*Pft + Qft*dPft_dVf)/(Pft**2))
            PMU_HIFasF['dMag'][filaIndexIF][to-1] = (1/(1+(Qft/Pft)**2))*((-dQft_dVt*Pft + Qft*dPft_dVt)/(Pft**2))
            PMU_HIFasF['Mat'] = np.append(PMU_HIFasF['dPhase'],PMU_HIFasF['dMag'],axis=1)
                        
            PMU_hxIMagF[filaIndexIF] =  ((Pft**2 +  Qft**2)**0.5)/Vfr
            PMU_hxIFasF[filaIndexIF] =(np.arctan2(-np.real(Qft),np.real(Pft))+Vfr_phase+np.pi)%(2*np.pi)-np.pi
                          
            filaIndexIF = filaIndexIF + 1

        if ramaActualIndex in ramasOT:
            # Potencias TO - FROM Para calculo de I 

            Ptf = hxPflowT[rama['ramaN']-1]
            Qtf = hxQflowT[rama['ramaN']-1]
            dPtf_dTf = HPflowT['dPhase'][rama['ramaN']-1][fr-1]
            dPtf_dTt = HPflowT['dPhase'][rama['ramaN']-1][to-1]
            dPtf_dVf = HPflowT['dMag'][rama['ramaN']-1][fr-1]
            dPtf_dVt = HPflowT['dMag'][rama['ramaN']-1][to-1]           
            dQtf_dTf = HQflowT['dPhase'][rama['ramaN']-1][fr-1]
            dQtf_dTt = HQflowT['dPhase'][rama['ramaN']-1][to-1]
            dQtf_dVf = HQflowT['dMag'][rama['ramaN']-1][fr-1]
            dQtf_dVt = HQflowT['dMag'][rama['ramaN']-1][to-1]
                        
            PMU_HIMagT['dPhase'][filaIndexIT][fr-1] = (1/Vto)*((1/(2*np.sqrt(Ptf**2 + Qtf**2))) * (2*Ptf*dPtf_dTf+2*Qtf*dQtf_dTf) )
            PMU_HIMagT['dPhase'][filaIndexIT][to-1] = (1/Vto)*((1/(2*np.sqrt(Ptf**2 + Qtf**2))) * (2*Ptf*dPtf_dTt+2*Qtf*dQtf_dTt) )
            PMU_HIMagT['dMag'][filaIndexIT][fr-1] = (1/Vto)*((1/(2*np.sqrt(Ptf**2 + Qtf**2))) * (2*Ptf*dPtf_dVt+2*Qtf*dQtf_dVt) )
            PMU_HIMagT['dMag'][filaIndexIT][to-1] = ((((1/(2*np.sqrt(Ptf**2 + Qtf**2))) * (2*Ptf*dPtf_dVf+2*Qtf*dQtf_dVf) )*Vto) - np.sqrt(Ptf**2 + Qtf**2)) /(Vto**2)
            PMU_HIMagT['Mat'] = np.append(PMU_HIMagT['dPhase'],PMU_HIMagT['dMag'],axis=1)
            
            PMU_HIFasT['dPhase'][filaIndexIT][fr-1] =  (1/(1+(Ptf/Qtf)**2))*((dPtf_dTf*Qtf - Ptf*dQtf_dTf)/(Qtf**2)) +1 
            PMU_HIFasT['dPhase'][filaIndexIT][to-1] = (1/(1+(Ptf/Qtf)**2))*((dPtf_dTt*Qtf - Ptf*dQtf_dTt)/(Qtf**2))
            PMU_HIFasT['dMag'][filaIndexIT][fr-1] = (1/(1+(Ptf/Qtf)**2))*((dPtf_dVf*Qtf - Ptf*dQtf_dVf)/(Qtf**2))
            PMU_HIFasT['dMag'][filaIndexIT][to-1] = (1/(1+(Ptf/Qtf)**2))*((dPtf_dVt*Qtf - Ptf*dQtf_dVt)/(Qtf**2))
            PMU_HIFasT['Mat'] = np.append(PMU_HIFasT['dPhase'],PMU_HIFasT['dMag'],axis=1)

            PMU_hxIMagT[filaIndexIT] =  ((Ptf**2 +  Qtf**2)**0.5)/Vto
            PMU_hxIFasT[filaIndexIT] =  (np.arctan2(-np.real(Qtf),np.real(Ptf))+Vto_phase+np.pi)%(2*np.pi)-np.pi 
            
            filaIndexIT = filaIndexIT + 1
            
        W_PMU_IMagF = pesosPMU*np.ones(len(PMU_hxIMagF))
        W_PMU_IFasF = pesosPMU*np.ones(len(PMU_hxIFasF))
        W_PMU_IMagT = pesosPMU*np.ones(len(PMU_hxIMagT))
        W_PMU_IFasT = pesosPMU*np.ones(len(PMU_hxIFasT))
#        
        
#######################################################################
##############################  Append ################################
#######################################################################
    
    #### H SCADA ####
    H = np.append(HPinj['Mat'], HQinj['Mat'],axis=0)
#    if HPflowF['Mat'] != []:
#        H = np.append(H, HPflowF['Mat'],axis=0)  
#    if HPflowT['Mat'] != []:    
#        H = np.append(H, HPflowT['Mat'],axis=0)
#    if HQflowF['Mat'] != []:    
#        H = np.append(H, HQflowF['Mat'],axis=0)
#    if HQflowT['Mat'] != []:    
#        H = np.append(H, HQflowT['Mat'],axis=0)
#    if HIMagF['Mat'] != []:    
#        H = np.append(H, HIMagF['Mat'],axis=0)
#    if HIMagT['Mat'] != []:     
#       H = np.append(H, HIMagT['Mat'],axis=0)    
#    H = np.append(H, HVMag['Mat'],axis=0)

#    #### H PMU ####
    if PMU_HVMag['Mat'] != [] :
        H = np.append(H,PMU_HVMag['Mat'],axis=0)
    if PMU_HVPhase['Mat'] != [] :
        H = np.append(H,PMU_HVPhase['Mat'],axis=0)
#    if PMU_HIMagF['Mat'] != [] :
#        H = np.append(H,PMU_HIMagF['Mat'],axis=0)
#    if PMU_HIFasF['Mat'] != [] :
#        H = np.append(H,PMU_HIFasF['Mat'],axis=0)    
#    if PMU_HIMagT['Mat'] != [] :
#        H = np.append(H,PMU_HIMagT['Mat'],axis=0)     
#    if PMU_HIFasT['Mat'] != [] :
#        H = np.append(H,PMU_HIFasT['Mat'],axis=0) 
##       
    
    #### hx SCADA ####
    hx = np.append(hxPinj,hxQinj)
#    hx = np.append(hx,hxPflowF)
#    hx = np.append(hx,hxPflowT)
#    hx = np.append(hx,hxQflowF)
#    hx = np.append(hx,hxQflowT)
#    hx = np.append(hx,hxIMagF)
#    hx = np.append(hx,hxIMagT)
#    hx = np.append(hx,hxVMag)


    #### hx PMU ####
    hx = np.append(hx,PMU_hxVMag)
    hx = np.append(hx,PMU_hxVPhase)
#    hx = np.append(hx,PMU_hxIMagF)
#    hx = np.append(hx,PMU_hxIFasF)
#    hx = np.append(hx,PMU_hxIMagT)
#    hx = np.append(hx,PMU_hxIFasT)

    #### W SCADA ####
    W = np.append(W_SCADA_Pinj,W_SCADA_Qinj)
#    W = np.append(W,W_SCADA_PflowF)
#    W = np.append(W,W_SCADA_PflowT)
#    W = np.append(W,W_SCADA_QflowF)
#    W = np.append(W,W_SCADA_QflowT)
#    W = np.append(W,W_SCADA_IMagF)
#    W = np.append(W,W_SCADA_IMagT)
#    W = np.append(W,W_SCADA_VMag)
    
    #### W PMU ####
    
    W = np.append(W, W_PMU_VMag)
    W = np.append(W, W_PMU_VPhase)
#    W = np.append(W, W_PMU_IMagF)
#    W = np.append(W, W_PMU_IFasF)
#    W = np.append(W, W_PMU_IMagT)
#    W = np.append(W, W_PMU_IFasT)


    W = np.diag(W) 
    return(H,hx,W)