from functions import *
from redElec import *
import sys
plt.close('all')
grabarGrafs = 0

# =============================================================================
# Preguntas 
# =============================================================================
# 1- Tiene sentido asumir que la función de costo tiene un único mínimo?

# =============================================================================
# ~SENAL IDEAL
# =============================================================================
signal = scipy.io.loadmat('phasors_3phasefault.mat')
signal['bus_v'] = np.delete(signal['bus_v'], (13), axis =0)
signal['bus_v'] = np.delete(signal['bus_v'], (600), axis =1)
signal['baseTiempo'] = np.array([x*1/60 for x in range(600)])


# =============================================================================
# ~ MEDICIONES
# =============================================================================
#### OBSERVABILIDAD
case = case13()
case_falla = case13()
case_falla['bus'][2][5] = case_falla['bus'][2][5] - 1e7

print(type(case))
sys.exit()
rE       = RedElec(case,pf_max_it=100)
rE_falla = RedElec(case_falla,pf_max_it=100)

ubicacionPMU                    = [10,13]
for flag in ubicacionPMU:
    if flag > rE.cantNodos:
        print("Error. Hay PMUs ubicados en nodos inexistentes")
        sys.exit()
ubicacionPMU.sort()


obs = {}
obs['cantNodos']    = rE.cantNodos
obs['cantRamas']    = rE.cantRamas
obs['nodosO']       = np.unique(ubicacionPMU)
obs['nodosU']       = np.array([x for x in range(1,rE.cantNodos+1) if x not in ubicacionPMU])
obs['ramasF_Obs']   = np.array([rama['ramaN'] for rama in rE.RamaLst if rama['from'] in obs['nodosO']])                    
obs['ramasF_NoObs'] = np.array([rama['ramaN'] for rama in rE.RamaLst if rama['from'] in obs['nodosU']])
obs['ramasT_Obs']   = np.array([rama['ramaN'] for rama in rE.RamaLst if rama['to'] in obs['nodosO']])
obs['ramasT_NoObs'] = np.array([rama['ramaN'] for rama in rE.RamaLst if rama['to'] in obs['nodosU']])

#### PARAMETROS DE SENSORES
intervaloPMU   = signal['baseTiempo'][1]   # en segundos
multiplierSCADA = 500
intervaloSCADA = multiplierSCADA*intervaloPMU # en segundos 
TVEPMU         = 1#1
TVESCADA       = 10#5

mediciones  = genMed(obs,rE,rE_falla,signal,TVEPMU,TVESCADA,intervaloPMU,intervaloSCADA)
meds        = genMed(obs,rE,rE_falla,signal,1,1,intervaloPMU,intervaloPMU)
Ynew        = Yreord(obs['nodosO'],obs['nodosU'],rE.Y)


# =============================================================================
# LOOP WLS con SCADA ONLY =====================================================
# =============================================================================

soportePLOT = 600
estadosWLSSCADA = np.ones((rE.cantNodos,mediciones['cantMuestras']))*(1+1j*0.1)

medsSCADA = np.append(mediciones['SCADA_Pinj'],mediciones['SCADA_Pinj'],axis=0)
medsSCADA = np.append(medsSCADA,mediciones['SCADA_PF'],axis=0)
medsSCADA = np.append(medsSCADA,mediciones['SCADA_PT'],axis=0)
medsSCADA = np.append(medsSCADA,mediciones['SCADA_QF'],axis=0)
medsSCADA = np.append(medsSCADA,mediciones['SCADA_QT'],axis=0)
medsSCADA = np.append(medsSCADA,mediciones['SCADA_IFMag'],axis=0)
medsSCADA = np.append(medsSCADA,mediciones['SCADA_ITMag'],axis=0)
medsSCADA = np.append(medsSCADA,mediciones['SCADA_VMag'],axis=0)

instantesSCADA = [x for x in range(600) if x%np.int(intervaloSCADA/intervaloPMU)==0] + [600]
estadosEstimadosSCADA = np.ones((13,len(instantesSCADA)),dtype=complex)
i = 0
for instante in instantesSCADA[0:-1]:
    print(instante)
    refFaseNodo = np.angle(signal['bus_v'][0,instante]) + np.angle(signal['bus_v'][0,instante])*(np.random.rand()-0.5)*0.08
    Vactual = {}
    Vactual['V'] = estadosWLSSCADA[:,instante].copy()
    Vactual['Mag'] = mediciones['SCADA_VMag'][:,instante]
    Vactual['Phase'] = np.angle(estadosWLSSCADA[:,instante]).copy()
    
    xk = [a for a in Vactual['Phase']][1:]
    xk = xk + [a for a in Vactual['Mag']]  
    [H,hx,W] = calcularHyhxOnlySCADA(Vactual,obs['nodosO'],[],[],rE)

    iteracionWLS=0 
    totalIteracionesWLS = 200
    pasoWLS = 0.1    
    while(iteracionWLS < totalIteracionesWLS):
        H = H[:,1:]
        HT = np.transpose(H)
        Dif_Med_hx = medsSCADA[:,instante] - hx                
        G = np.matmul(W,H).real
        G = np.matmul(HT,G).real       
        g = np.matmul(-HT,W).real        
        g = (g.dot(Dif_Med_hx)).real 
        xkp1 = (xk - pasoWLS*np.linalg.inv(G).dot(g)   ).tolist()
        
        Vactual['Phase'] = np.array([refFaseNodo] + xkp1[:rE.cantNodos-1])    
        Vactual['Mag'] = np.array(xkp1[rE.cantNodos-1:])
        Vactual['V'] = np.array(Vactual['Mag'])*(np.cos(Vactual['Phase'])+1j*(np.sin(Vactual['Phase'])))
        
        [H,hx,W]=calcularHyhxOnlySCADA(Vactual,obs['nodosO'],[],[],rE)                   
        xk = xkp1
        iteracionWLS = iteracionWLS +1
        
    estadosEstimadosSCADA[:,i] = Vactual['V']
    i = i +1


for index in range(len(instantesSCADA)-1):
    print(index)
    inicial = instantesSCADA[index]
    final = instantesSCADA[index+1]-1  
    aux = np.reshape(estadosEstimadosSCADA[:,index],(13,1))
    if index != int(500/multiplierSCADA): # 500 es un valor harcodeado
        estadosWLSSCADA[:,inicial:final+1] = np.repeat(aux,multiplierSCADA,axis=1) 
    else:
        estadosWLSSCADA[:,inicial:instantesSCADA[-1]] = np.repeat(aux,instantesSCADA[-1]-inicial,axis=1)    

TVEOnlySCADA = (np.abs(signal['bus_v'] - estadosWLSSCADA)/np.abs(signal['bus_v'] ))*100


##PLOT COMPARATIVA IDEAL
plt.figure()
plt.suptitle('Comparativa módulo real de tensión con estimación')  
ii = 0 
tiempo = mediciones['tiempo']
for i in range(obs['cantNodos']):     
    plt.subplot(3,5,i+1)
    plt.plot(tiempo[:soportePLOT],np.abs(signal['bus_v'][i,:soportePLOT]))
    plt.plot(tiempo[:soportePLOT],np.abs(estadosWLSSCADA[i,:soportePLOT]),'k',markersize=3)
    plt.gca().set_title("Nodo: " + str(i+1))
    plt.grid()
plt.subplots_adjust(wspace=0.6, hspace=0.5)

if grabarGrafs:
    plt.savefig("compModuloOnlyScada"+str(int(intervaloSCADA))+"S.pdf")


plt.figure()
plt.suptitle('Comparativa fase real de tensión con estimación')  
ii = 0 
tiempo = mediciones['tiempo']
for i in range(obs['cantNodos']):     
    plt.subplot(3,5,i+1)
    plt.plot(tiempo[:soportePLOT],np.angle(signal['bus_v'][i,:soportePLOT]))
    plt.plot(tiempo[:soportePLOT],np.angle(estadosWLSSCADA[i,:soportePLOT]),'k',markersize=3)    
    plt.gca().set_title("Nodo: " + str(i+1))
    plt.grid()
plt.subplots_adjust(wspace=0.6, hspace=0.5)
if grabarGrafs:
    print("hola")
    plt.savefig("compFaseOnlyScada"+str(int(intervaloSCADA))+"S.pdf")


##PLOT TVE
plt.figure()
plt.suptitle('TVE Tensión Nodos')  
ii = 0 
tiempo = mediciones['tiempo']
for i in range(obs['cantNodos']):     
    plt.subplot(3,5,i+1)
    plt.plot(tiempo[:soportePLOT],TVEOnlySCADA[i,:soportePLOT])
    plt.gca().set_title("Nodo: " + str(i+1))
    plt.grid()
    plt.ylim([-0.1,200])
plt.subplots_adjust(wspace=0.6, hspace=0.5)
if grabarGrafs:
    plt.savefig("TVEOnlyScada"+str(int(intervaloSCADA))+"S.pdf")

## PLOT Zoom Nodo  8 
nodoN = 7
plt.figure()
plt.suptitle('Análisis en Nodo' + str(nodoN+1))
plt.subplot(1,3,1)
plt.gca().set_title("Módulo")
plt.plot(tiempo[:soportePLOT],np.abs(signal['bus_v'][nodoN,:soportePLOT]))
plt.plot(tiempo[:soportePLOT],np.abs(estadosWLSSCADA[nodoN,:soportePLOT]),'k',markersize=3)
plt.grid()
plt.subplot(1,3,2)
plt.gca().set_title("Fase")
plt.plot(tiempo[:soportePLOT],np.angle(signal['bus_v'][nodoN,:soportePLOT]))
plt.plot(tiempo[:soportePLOT],np.angle(estadosWLSSCADA[nodoN,:soportePLOT]),'k',markersize=3)
plt.grid()
plt.subplot(1,3,3)
plt.gca().set_title("TVE")
plt.plot(tiempo[:soportePLOT],TVEOnlySCADA[nodoN,:soportePLOT])
plt.grid()
plt.subplots_adjust(wspace=0.4, hspace=0.5)
if grabarGrafs:
    plt.savefig("nodo"+str(nodoN +1)+"OnlySCADA"+str(int(intervaloSCADA))+"S.pdf")


## PLOT Zoom Nodo  3 
nodoN = 2
plt.figure()
plt.suptitle('Análisis en Nodo'  + str(nodoN+1))
plt.subplot(1,3,1)
plt.gca().set_title("Módulo")
plt.plot(tiempo[:soportePLOT],np.abs(signal['bus_v'][nodoN,:soportePLOT]))
plt.plot(tiempo[:soportePLOT],np.abs(estadosWLSSCADA[nodoN,:soportePLOT]),'k',markersize=3)
plt.grid()
plt.subplot(1,3,2)
plt.gca().set_title("Fase")
plt.plot(tiempo[:soportePLOT],np.angle(signal['bus_v'][nodoN,:soportePLOT]))
plt.plot(tiempo[:soportePLOT],np.angle(estadosWLSSCADA[nodoN,:soportePLOT]),'k',markersize=3)
plt.grid()
plt.subplot(1,3,3)
plt.gca().set_title("TVE")
plt.plot(tiempo[:soportePLOT],TVEOnlySCADA[nodoN,:soportePLOT])
plt.grid()
plt.ylim([-0.1,200])
plt.subplots_adjust(wspace=0.4, hspace=0.5)
if grabarGrafs:
    plt.savefig("nodo"+ str(nodoN +1) + "OnlySCADA"+str(int(intervaloSCADA))+"S.pdf")


## PLOT Zoom Nodo  12
nodoN = 11
plt.figure()
plt.suptitle('Análisis en Nodo'  + str(nodoN +1))
plt.subplot(1,3,1)
plt.gca().set_title("Módulo")
plt.plot(tiempo[:soportePLOT],np.abs(signal['bus_v'][nodoN,:soportePLOT]))
plt.plot(tiempo[:soportePLOT],np.abs(estadosWLSSCADA[nodoN,:soportePLOT]),'k',markersize=3)
plt.grid()
plt.subplot(1,3,2)
plt.gca().set_title("Fase")
plt.plot(tiempo[:soportePLOT],np.angle(signal['bus_v'][nodoN,:soportePLOT]))
plt.plot(tiempo[:soportePLOT],np.angle(estadosWLSSCADA[nodoN,:soportePLOT]),'k',markersize=3)
plt.grid()
plt.subplot(1,3,3)
plt.gca().set_title("TVE")
plt.plot(tiempo[:soportePLOT],TVEOnlySCADA[nodoN,:soportePLOT])
plt.grid()
plt.subplots_adjust(wspace=0.4, hspace=0.5)
if grabarGrafs:
    plt.savefig("nodo"+str(nodoN +1)+"OnlySCADA"+str(int(intervaloSCADA))+"S.pdf")


## PLOT señal ideal 

plt.figure()
plt.subplot(1,2,1)
i = 2
soportePLOT2 = np.int(soportePLOT/2)
plt.plot(tiempo[:soportePLOT2],np.abs(signal['bus_v'][i,:soportePLOT2]))
plt.gca().set_title("Módulo Nodo: " + str(i+1))
plt.grid()
plt.subplot(1,2,2)
plt.plot(tiempo[:soportePLOT2],np.angle(signal['bus_v'][i,:soportePLOT2]))
plt.gca().set_title("Fase Nodo: " + str(i+1))
plt.grid()
if grabarGrafs:  
    plt.savefig("Nodo3.pdf")


plt.figure()
plt.suptitle('Módulo de la señal real')
for i in range(obs['cantNodos']):     
    plt.subplot(3,5,i+1)
    plt.plot(tiempo[:soportePLOT],np.abs(signal['bus_v'][i,:soportePLOT]))
    plt.gca().set_title("Nodo: " + str(i+1))
    plt.grid()
plt.subplots_adjust(wspace=0.6, hspace=0.5)
plt.savefig("NodosCase13Modulo.pdf")
plt.figure()
plt.suptitle('Fase de la señal real')  
ii = 0 
tiempo = mediciones['tiempo']
for i in range(obs['cantNodos']):     
    plt.subplot(3,5,i+1)
    plt.plot(tiempo[:soportePLOT],np.angle(signal['bus_v'][i,:soportePLOT]))
    plt.gca().set_title("Nodo: " + str(i+1))
    plt.grid()
plt.subplots_adjust(wspace=0.6, hspace=0.5)
if grabarGrafs:
    plt.savefig("NodosCase13Fase.pdf")


###############################################################################
###############################################################################
###############################################################################