import numpy as np
import matplotlib.pyplot as plt
pi = np.pi
e = np.e


mu = 2
sigma = 2
x = np.linspace(mu -3*sigma,mu +3*sigma,100)


exponente  = -np.power((x-mu),2)/(2*sigma^2)
constante = 1/(sigma*np.sqrt(2*pi))

y = constante*np.power(e,exponente)
plt.close()
plt.figure()
plt.plot(x,y)
plt.grid()
plt.title(r'$\phi_{(2,4)}(x)$')

fname='normal1.pdf'
plt.savefig(fname)