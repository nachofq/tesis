import networkx as nx
import matplotlib.pyplot as plt 
import cmath
import numpy as np


class Topology:
    """ Clase para handling de elementos de la topología de red seleccionada"""

    def __init__(self,case = ""):
        """ case:  diccionario con el siguiente formato:
            {
            'version': <class 'str'>,
            'baseMVA': <class 'int'>,
            'bus':  <class 'numpy.ndarray'>,
            'gen': <class 'numpy.ndarray'>,
            'branch': <class 'numpy.ndarray'>
            }
        """
        ref = PyPower_Refs()
        self.ref = ref


        # <Loading info from case>
        self.case               = case
        self.baseMVA            = case['baseMVA']
        self.buses              = case['bus']
        self.generators         = case['gen']
        self.branches           = case['branch']
        # </>

        # <Counting nodes from 1>
        if self.buses[0,ref.BUS_i] == 0:  
            self.buses[:,ref.BUS_i]                     = self.buses[:,ref.BUS_i] + 1 
            self.generators[:,ref.GEN_bus_i]            = self.generators[:,ref.GEN_bus_i] + 1
            self.branches[:,ref.BR_fbus:ref.BR_tbus+1]  = self.branches[:,ref.BR_fbus:ref.BR_tbus+1] + 1
        # </>

        # <Changing  tap values from 0 to 1>
        for branch  in self.branches:
            if branch[ref.BR_tap]==0:
                branch[ref.BR_tap] = 1
        # </>

        self.nBuses        = len(self.buses)
        self.nBranches     = len(self.branches)


        [self.Ybus,self.Yf,self.Yt,self.Cf,self.Ct,self.Ysh]  = self.YbusCalculator()
       


    def observabilityCalculator(self):
        pass

    def YbranchCalculator(self,branchIndex):
        ref         = PyPower_Refs()
        self.ref    = ref
        branchPos   =  self.branches[:,ref.BR_i].tolist().index(branchIndex)

        r       = self.branches[branchPos,ref.BR_R]
        x       = self.branches[branchPos,ref.BR_X]
        ys      = 1 / (r + 1j*x)
        bc      = self.branches[branchPos,ref.BR_B]
        tao     = self.branches[branchPos,ref.BR_tap]
        shift   = self.branches[branchPos,ref.BR_shift]

        yff = (ys + 1j*bc/2)*(1/tao**2)
        yft = -ys * (1/(tao*cmath.exp(-1j*shift)))
        ytf = -ys * (1/(tao*cmath.exp(1j*shift)))
        ytt = ys + 1j*(bc/2)

        Ybr = np.array([[yff,yft],[ytf,ytt]])

        return  Ybr

    def YshuntCalculator(self):
        ref = PyPower_Refs()
        Ysh = np.array([])
        for bus in self.buses:
            ysh = (bus[ref.BUS_Gs] + 1j*bus[ref.BUS_Bs]) / self.baseMVA
            Ysh = np.append(Ysh,ysh)
        return Ysh

    def YbusCalculator(self):
        ref = PyPower_Refs()
        Yff = np.array([])
        Yft = np.array([])
        Ytf = np.array([])
        Ytt = np.array([])
        Cf  = np.zeros([self.nBranches,self.nBuses])
        Ct  = np.zeros([self.nBranches,self.nBuses])

        for  branch in self.branches:
            Ybr = self.YbranchCalculator(branch[ref.BR_i])
            Yff = np.append(Yff,Ybr[0,0])
            Yft = np.append(Yft,Ybr[0,1])
            Ytf = np.append(Ytf,Ybr[1,0])
            Ytt = np.append(Ytt,Ybr[1,1])

            Cf[branch[ref.BR_i].astype(int)-1,branch[ref.BR_fbus].astype(int)-1] = 1
            Ct[branch[ref.BR_i].astype(int)-1,branch[ref.BR_tbus].astype(int)-1] = 1

        Ysh = np.diag(self.YshuntCalculator())
        
        Yf  = np.matmul(np.diag(Yff),Cf) + np.matmul(np.diag(Yft),Ct)
        Yt  = np.matmul(np.diag(Ytf),Cf) + np.matmul(np.diag(Ytt),Ct)
        Ybus = np.matmul(np.transpose(Cf),Yf) + np.matmul(np.transpose(Ct),Yt) + Ysh
        return [Ybus,Yf,Yt,Cf,Ct,Ysh]


    def fromNodeBranches(self,node):
        """ Returns the branches in which node is the from node """
        branches = np.array([])
        for branch in self.branches:
            if branch[self.ref.BR_fbus].astype(int) == node:
                branches = np.append(branches,branch[self.ref.BR_i].astype(int))
        return branches

    def toNodeBranches(self,node):
        """ Returns the branches in which node is the from node """
        branches = np.array([])
        for branch in self.branches:
            if branch[self.ref.BR_tbus].astype(int) == node:
                branches = np.append(branches,branch[self.ref.BR_i])
        return branches

    def printNetwork(self):
        ref = PyPower_Refs()

        G = nx.Graph()
        
        G.add_nodes_from(self.buses[:,ref.BUS_i].astype(int))
       # G.add_edges_from(self.branches[:,ref.BR_fbus:ref.BR_tbus+1].astype(int))
        for edge in self.branches:
            Ybr = np.around(self.YbranchCalculator(edge[ref.BR_i]),4)
            G.add_edge(edge[ref.BR_fbus],edge[ref.BR_tbus],r=Ybr)
            edge_labels  = nx.get_edge_attributes(G,'r')

        pos = nx.spring_layout(G)
        nx.draw_networkx_edge_labels(G,pos,edge_labels = edge_labels, font_size=6)

        nx.draw(G,pos, with_labels=True, node_color = 'r', node_size = 350, alpha = 1)
        
        generatorList = self.generators[:,0].astype(int)
        nx.draw_networkx_nodes(G,pos, generatorList, node_color='g',node_size=650,alpha=1)

        plt.show()
    

class PyPower_Refs:
    def __init__(self):
        # <Bus elements>
        self.BUS_i        = 0
        self.BUS_type     = 1
        self.BUS_Pd       = 2
        self.BUS_Qd       = 3
        self.BUS_Gs       = 4
        self.BUS_Bs       = 5
        self.BUS_area     = 6
        self.BUS_Vm       = 7
        self.BUS_Va       = 8
        self.BUS_baseKV   = 9
        self.BUS_zone     = 10
        self.BUS_Vmax     = 11
        self.BUS_Vmin     = 12
        # </>
        
        # <Branch elements>
        self.BR_i           = 0
        self.BR_fbus        = 1
        self.BR_tbus        = 2
        self.BR_R           = 3
        self.BR_X           = 4
        self.BR_B           = 5
        self.BR_rateA       = 6
        self.BR_rateB       = 7
        self.BR_rateC       = 8
        self.BR_tap         = 9
        self.BR_shift       = 10
        self.BR_status      = 11
        self.BR_angMin      = 12
        self.BR_angMax      = 13
        # </>

        # <Gen elements>
        self.GEN_bus_i       = 0
        self.GEN_Pg          = 1
        self.GEN_Qg          = 2
        self.GEN_Qmax        = 3
        self.GEN_Qmin        = 4
        self.GEN_Vg          = 5
        self.GEN_mBase       = 6
        self.GEN_status      = 7
        self.GEN_Pmax        = 8
        self.GEN_Pmin        = 9
        self.GEN_Pc1         = 10
        self.GEN_Pc2         = 11
        self.GEN_Qc1min      = 12
        self.GEN_Qc1max      = 13
        self.GEN_Qc2min      = 14
        self.GEN_Qc2max      = 15
        self.GEN_ramp_agc    = 16
        self.GEN_ramp_10     = 17
        self.GEN_ramp_30     = 18
        self.GEN_ramp_q      = 19
        self.GEN_apf         = 20
        # </>
        

