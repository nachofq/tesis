import os 

def whereAmI():
    """
    Devuelve el currentDirectory para tener una referencia global dentro del código.
    """
    return os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

