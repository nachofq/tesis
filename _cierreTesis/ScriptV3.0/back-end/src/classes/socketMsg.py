import socket
import time
import pickle
import numpy as np

class SocketMsg:

    #Globals
    MESSAGE_HEADERSIZE      = 10
    BIND_ADDRESS            = 1111

    def __init__(self,socketType="",dataArray: np.ndarray = []):

        if socketType not in ["server", "client"]:
            raise Exception("You must define socketType=<server|client>")
        self.socketType = socketType

        # Server Init
        if self.socketType == "server":
            if dataArray == [] or type(dataArray) != np.ndarray:
                raise Exception("You must define a numpy array to be streamed")

            self.dataArray = dataArray
            self.socketSrv  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socketSrv.bind((socket.gethostname(),self.BIND_ADDRESS))
            self.socketSrv.listen(5)
            print("Socket Server listening...")
            self.socketServer()

        #Client Init
        if self.socketType == "client":
            self.socketCli = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socketCli.connect((socket.gethostname(),self.BIND_ADDRESS))
            

    def socketServer(self):
        """
        socketServer:  Monta  un servidor para el envío uno a uno de los elementos de un np.ndarray.
        El tiempo de envío de los elementos será información contenida en el elemento.
        """
        while True:
            try:    
                clientsocket, address = self.socketSrv.accept()
                print(f"Connection from {address} has been established")
                objectToSend = {'type': "Message", 'data': 'Welcome to the Server!', 'timestamp': f'{time.time()}' }
                msg = pickle.dumps(objectToSend)
                msg = bytes(f'{len(msg):<{self.MESSAGE_HEADERSIZE}}','utf-8') + msg
                clientsocket.send(msg)
                # Logica de envio de mediciones!

                for dataElement in self.dataArray:
                    time.sleep(3)
                    objectToSend = {'type': "Data", 'data': dataElement, 'timestamp': f'{time.time()}'}
                    msg = pickle.dumps(objectToSend)
                    msg = bytes(f'{len(msg):<{self.MESSAGE_HEADERSIZE}}','utf-8') + msg
                    clientsocket.send(msg)

            # Exceptions        
            except BrokenPipeError:
                print(f"Connection from {address} has been closed")
                clientsocket.close()
            except KeyboardInterrupt:
                print("\nTerminating server...")
                self.socketSrv.shutdown(socket.SHUT_RDWR)
                self.socketSrv.close()
                break
            except ConnectionResetError:
                print(f"Connection from {address} has been closed")
                clientsocket.close()


    def socketClient(self):
        """
        SocketClient: Simula  el módulo del centro de control encargado de recolectar las  mediciones de los sensores.
        """
        try:
            while True:
                newMsg = True
                msgBuffer = b''
                while True:                 
                        msg = self.socketCli.recv(16)
                        if newMsg:
                            msgLen = int(msg[:self.MESSAGE_HEADERSIZE])
                            newMsg = False
                        msgBuffer += msg
                        if  len(msgBuffer)-self.MESSAGE_HEADERSIZE == msgLen:
                            #msgSize = int(msgBuffer[:self.MESSAGE_HEADERSIZE])
                            msgReceived = pickle.loads(msgBuffer[self.MESSAGE_HEADERSIZE:])
                            newMsg = True
                            msgBuffer = b''
                            yield msgReceived

        except KeyboardInterrupt:
            print("\nTerminating client...")
            self.socketCli.shutdown(socket.SHUT_RDWR)
            self.socketCli.close()