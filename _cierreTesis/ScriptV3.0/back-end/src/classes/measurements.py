import socket
import math
import time
import pickle
import scipy.io
import numpy  as np
import sys
from .topology import Topology
from . import utilsBackEnd
import matplotlib.pyplot as plt 

class Measurements:
    def __init__(self, trueSignalPath="",idealSignalSampleRate="", topology="", sensors=""):

        # <Carga de la señal ideal>
        if trueSignalPath == "":
            raise Exception("You must define a trueSignalPath .mat file")
        self.trueSignalPath = trueSignalPath
        self.trueSignalLoader()
        if self.trueSignal.shape[0] != topology.nBuses:
            raise Exception("Number of loaded signals is different  to amount of topology buses")
        # </>

        # <Carga de la topología>
        if topology == "":
            raise Exception("You must define a topology instance")
        self.topology = topology
        # </>

        if sensors == "":
            raise Exception("You must define location and characteristics of you sensor network")
        self.sensors = sensors
       
        # <Generación valores ideales>
        self.idealSignalSampleRate = idealSignalSampleRate
        self.idealValuesGenerator()
        # </>

        # <Generación mediciones>
        self.measurementGenerator(self.sensors)
        # </>

    def trueSignalLoader(self):
        """
        Para simular mediciones se requiere una señal ideal conocida. Cuando la señal ideal sea del fasor de tensión este
        método se encargará de cargarla y formatearla. 
        Si se utiliza path, el formato del archivo deberá ser .mat
        Cada columna es una nueva muestra
        ------------------------
        #1  #2  #3  #4  ... #N
        v11 v12 v13 v14 ... v1N
        .     .
        .           .
        .                 .
        VM1 vM2 vM3 vM4 ... vMN
        ------------------------
        N nodos, M muestras (tiempo discreto)

        """
        try:
            signalAux            = scipy.io.loadmat(self.trueSignalPath)
            self.trueSignal      = signalAux['bus_v'][:-1,:-1] #Removing  last row and column. No data there
            self.nSamples        = np.shape(self.trueSignal)[1] 
            print(f"Data from {self.trueSignalPath} loaded")
        except FileNotFoundError:
            print(f"{self.trueSignalPath} not found...")
            sys.exit()
            
    def idealValuesGenerator(self, save=0):
        """
        Nota: Los valores de SBus(t) mapean las potencias netas en 
        cada nodo. Es decir SBus(t) = Sgen(t) - Sloads(t)
        
        SBus(t) se calcula de forma dinámica utilizando V(t) y la topología.
        No se utiliza para su cómputo información del case pues esta es estática.

        """
        # <señales BUS> 
        ideal_V             = self.trueSignal
        ideal_IBus          = np.matmul(self.topology.Ybus,ideal_V)
        ideal_IShunt        = np.matmul(self.topology.Ysh,ideal_V)
        ideal_SBus          = np.empty([self.topology.nBuses,0]) #  SBus incluye  SShunt
        ideal_SShunt        = np.empty([self.topology.nBuses,0]) #  Puede ser util tener SShunt separada de SBus

        for i in range(self.nSamples):
            SBus_aux        = np.matmul(np.diag(ideal_V[:,i]),np.conj(ideal_IBus[:,i]))
            SShunt_aux      = np.matmul(np.diag(ideal_V[:,i]),np.conj(ideal_IShunt[:,i]))
            ideal_SBus      = np.column_stack([ideal_SBus, SBus_aux])
            ideal_SShunt    = np.column_stack([ideal_SShunt, SShunt_aux])
        #</>

        # <señales Branches>
        ideal_IBranchF      = np.matmul(self.topology.Yf,ideal_V)
        ideal_IBranchT      = np.matmul(self.topology.Yt,ideal_V)
        ideal_SBranchF      = np.empty([self.topology.nBranches,0])
        ideal_SBranchT      = np.empty([self.topology.nBranches,0])

        """
        Antes  lo calculaba así, pero encontré en matpower una forma más reducida de calcular las potencias de rama.
        for i in range(self.nSamples):

            # 
            fromArray       = np.array([branch[self.topology.ref.BR_fbus] for branch in self.topology.branches]).astype(int)
            VfromArray      = np.array([ideal_V[k-1,i] for k in fromArray]) 
            toArray         = np.array([branch[self.topology.ref.BR_tbus] for branch in self.topology.branches]).astype(int)
            VtoArray        = np.array([ideal_V[k-1,i] for k in toArray]) 

            SBranchF_aux    = np.matmul(np.diag(VfromArray),np.conj(ideal_IBranchF[:,i]))
            SBranchT_aux    = np.matmul(np.diag(VtoArray),np.conj(ideal_IBranchT[:,i]))

            ideal_SBranchF  = np.column_stack([ideal_SBranchF, SBranchF_aux])     
            ideal_SBranchT  = np.column_stack([ideal_SBranchT, SBranchT_aux])
        """      

        ideal_SBranchF      = np.empty([self.topology.nBranches,0])
        ideal_SBranchT      = np.empty([self.topology.nBranches,0])
        for i in range(self.nSamples):
            Vaux = ideal_V[:,i]
            SBranchF_aux = np.matmul(  np.diag(   np.matmul(self.topology.Cf,Vaux)  ), np.conj(ideal_IBranchF[:,i]))
            SBranchT_aux = np.matmul(  np.diag(   np.matmul(self.topology.Ct,Vaux)  ), np.conj(ideal_IBranchT[:,i]))

            ideal_SBranchF = np.column_stack([ideal_SBranchF, SBranchF_aux]) 
            ideal_SBranchT = np.column_stack([ideal_SBranchT, SBranchT_aux]) 
        
        # </>


        self.idealValues = {'samplingRate':1,'ideal_V':ideal_V,'ideal_IBus':ideal_IBus,'ideal_SBus':ideal_SBus,'ideal_IShunt': ideal_IShunt , 'ideal_SShunt': ideal_SShunt, 'ideal_IBranchF': ideal_IBranchF, 'ideal_IBranchT': ideal_IBranchT, 'ideal_SBranchF': ideal_SBranchF, 'ideal_SBranchT': ideal_SBranchT}


        if save == 1:
            scipy.io.savemat('../data/ideal_V', mdict={'ideal_V': ideal_V})
            scipy.io.savemat('../data/ideal_IBus',mdict={'ideal_IBus': ideal_IBus})
            scipy.io.savemat('../data/ideal_SBus',mdict={'ideal_SBus': ideal_SBus})
            scipy.io.savemat('../data/ideal_IBranchF',mdict={'ideal_IBranchF': ideal_IBranchF})
            scipy.io.savemat('../data/ideal_IBranchT',mdict={'ideal_IBranchT': ideal_IBranchT})
            scipy.io.savemat('../data/ideal_SBranchF',mdict={'ideal_SBranchF': ideal_SBranchF})
            scipy.io.savemat('../data/ideal_SBranchT',mdict={'ideal_SBranchT': ideal_SBranchT})

    def measurementGenerator(self,sensorNetwok=""):
        
        if  sensorNetwok == "":
            raise Exception("you must define a sensor network to generate measurements")

        # Generación de muestras obtenidas por la red de PMUs
        [mPMU_DirectV,mPMU_IndirectV]  = self.generateMeasurementsPMU_V()   # [Bus, [MeasurementArray]] 
        [mPMU_IF, mPMU_IT]             = self.generateMeasurementsPMU_I()   # [FromBus,ToBus,[I_MeasurementArray]]
        [mPMU_SF, mPMU_ST]             = self.generateMeasurementsPMU_S()   # [FromBus,ToBus,[S_MeasurementArray]] 

        # Testing
        #self.tveCalculator(mPMU_DirectV[0][1], self.idealValues["ideal_V"][0])
        #self.signalCompare(mPMU_DirectV[0][1], self.idealValues["ideal_V"][0])
        #self.tveCalculator(mPMU_IF[0][2], self.idealValues["ideal_IBranchF"][0])
        #self.signalCompare(mPMU_IF[0][2], self.idealValues["ideal_IBranchF"][0])
        #self.tveCalculator(mPMU_SF[0][2], self.idealValues["ideal_SBranchF"][0])
        #self.signalCompare(mPMU_SF[0][2], self.idealValues["ideal_SBranchF"][0])

        # Generación de muestras obtenidas por la red de PMUs
        mSCADA_VMag                      = self.generateMeasurementsSCADA_VMag()   # [Bus, [MeasurementArray]] 
        [mSCADA_IFMag, mSCADA_ITMag]     = self.generateMeasurementsSCADA_IMag()   # [FromBus,ToBus,[I_MeasurementArray]]
        [mSCADA_SF, mSCADA_ST]           = self.generateMeasurementsSCADA_S()   # [FromBus,ToBus,[S_MeasurementArray]] 

        # Testing
        self.tveCalculator(self.idealValues["ideal_V"][0],mSCADA_VMag[0][1])
        self.signalCompare(self.idealValues["ideal_V"][0],mSCADA_VMag[0][1])
        #self.tveCalculator(mPMU_IF[0][2], self.idealValues["ideal_IBranchF"][0])
        #self.signalCompare(mPMU_IF[0][2], self.idealValues["ideal_IBranchF"][0])
        #self.tveCalculator(mPMU_SF[0][2], self.idealValues["ideal_SBranchF"][0])
        #self.signalCompare(mPMU_SF[0][2], self.idealValues["ideal_SBranchF"][0])


        sys.exit()
        
        """
        Este método se encarga de generar mediciones de todas las variables eléctricas presentes en la topología. Este método requiere conocer la topología de red. Referirse a la clase Topology
        para más detalles.
        """
        pass

    def generateMeasurementsPMU_V(self):
        ideal_V                 = self.idealValues['ideal_V']

        directV      =  []
        indirectV    =  []

        # Direct voltage measurements
        for bus in self.sensors.arrayPMU:
            busNumber           = bus[0]
            TVE                 = bus[1].TVE
            noiseV  = self.circularNoiseGenerator(self.nSamples,ideal_V[busNumber-1],TVE=TVE)
            noisySignalV = ideal_V[busNumber-1] + noiseV           
            directV += [[busNumber, noisySignalV]]


        # Indirect voltage measurements
        for bus in self.sensors.arrayObsPMU:
            busNumber       = bus[0]
            busNeigbors     = bus[1]
            for neighbor in busNeigbors:
                noiseV  = self.circularNoiseGenerator(self.nSamples,ideal_V[busNumber-1],TVE=TVE)
                noisySignalV = ideal_V[neighbor-1] + noiseV          
                indirectV  +=  [[neighbor,noisySignalV]]
                          
        return [directV,indirectV]
        
    def generateMeasurementsPMU_I(self):
        ideal_IF = self.idealValues["ideal_IBranchF"]
        ideal_IT = self.idealValues["ideal_IBranchT"]

        measureIF   = [] 
        measureIT   = []

        for bus in self.sensors.arrayPMU:

            busNumber           = bus[0]
            TVE                 = bus[1].TVE

            for branch  in self.topology.branches:
                branchN     = branch[self.topology.ref.BR_i].astype(int)
                fbus        = branch[self.topology.ref.BR_fbus]
                tbus        = branch[self.topology.ref.BR_tbus]

                # Generación de mediciones Ifrom (El sensor está situado en un  nodo from)

                if busNumber == fbus:
                    noiseIF = self.circularNoiseGenerator(self.nSamples,ideal_IF[branchN-1],TVE=TVE)
                    noisySignalIF = ideal_IF[branchN-1] + noiseIF
                    measureIF += [[fbus,tbus, noisySignalIF]]
                    
                #  Generación de mediciones Ito (El sensor está situado en un nodo to)
                if busNumber == tbus:
                    noiseIT = self.circularNoiseGenerator(self.nSamples,ideal_IT[branchN-1],TVE=TVE)
                    noisySignalIT = ideal_IT[branchN-1] + noiseIT
                    measureIT += [[fbus,tbus, noisySignalIT]]

        return  [measureIF,measureIT]

    def generateMeasurementsPMU_S(self):
        ideal_SF = self.idealValues["ideal_SBranchF"]
        ideal_ST = self.idealValues["ideal_SBranchT"]

        measureSF   = [] 
        measureST   = []

        for bus in self.sensors.arrayPMU:

            busNumber           = bus[0]
            TVE                 = bus[1].TVE

            for branch  in self.topology.branches:
                branchN     = branch[self.topology.ref.BR_i].astype(int)
                fbus        = branch[self.topology.ref.BR_fbus]
                tbus        = branch[self.topology.ref.BR_tbus]

                # Generación de mediciones Ifrom (El sensor está situado en un  nodo from)

                if busNumber == fbus:
                    noiseSF = self.circularNoiseGenerator(self.nSamples,ideal_SF[branchN-1],TVE=TVE)
                    noisySignalSF = ideal_SF[branchN-1] + noiseSF
                    measureSF += [[fbus,tbus, noisySignalSF]]
                    
                #  Generación de mediciones Ito (El sensor está situado en un nodo to)
                if busNumber == tbus:
                    noiseST = self.circularNoiseGenerator(self.nSamples,ideal_ST[branchN-1],TVE=TVE)
                    noisySignalST = ideal_ST[branchN-1] + noiseST
                    measureST += [[fbus,tbus, noisySignalST]]

        return  [measureSF,measureST]

    def generateMeasurementsSCADA_VMag(self):
        # ARREGLAR  MEDICIONES SCADA
        ideal_V                 = self.idealValues['ideal_V']
        reportTime              = self.sensors.SCADAReportTime # Report time is Global for  all sensors
        downSamplingRateSCADA   = reportTime/self.idealSignalSampleRate
        sampleTimeArraySCADA    = [int(x*downSamplingRateSCADA) for x in range(self.nSamples) if x*downSamplingRateSCADA <= self.nSamples]
        measureVmag             =  []

        for bus in self.sensors.arraySCADA:
            busNumber           = bus[0]
            TVE                 = bus[1].TVE
            noiseV  = self.circularNoiseGenerator(self.nSamples,ideal_V[busNumber-1],TVE=TVE)
            noisySignalV = ideal_V[busNumber-1] + noiseV
            measureVmag += [[busNumber, noisySignalV]]

        for time  in sampleTimeArraySCADA:
            for row in range(np.shape(measureVmag)[0]):
                measureVmag[row][1][time:] = measureVmag[row][1][time]
        return measureVmag

    def generateMeasurementsSCADA_IMag(self):
        ideal_IF = self.idealValues["ideal_IBranchF"]
        ideal_IT = self.idealValues["ideal_IBranchT"]

        measureIFMag   = [] 
        measureITMag   = []

        for bus in self.sensors.arraySCADA:
            busNumber           = bus[0]
            TVE                 = bus[1].TVE
            for branch  in self.topology.branches:
                branchN     = branch[self.topology.ref.BR_i].astype(int)
                fbus        = branch[self.topology.ref.BR_fbus]
                tbus        = branch[self.topology.ref.BR_tbus]
                # Generación de mediciones Ifrom (El sensor está situado en un  nodo from)

                if busNumber == fbus:
                    noiseIF = self.circularNoiseGenerator(self.nSamples,ideal_IF[branchN-1],TVE=TVE)
                    noisySignalIF = np.abs(ideal_IF[branchN-1] + noiseIF)
                    measureIFMag += [[fbus,tbus, noisySignalIF]]
                    
                #  Generación de mediciones Ito (El sensor está situado en un nodo to)
                if busNumber == tbus:
                    noiseIT = self.circularNoiseGenerator(self.nSamples,ideal_IT[branchN-1],TVE=TVE)
                    noisySignalIT = np.abs(ideal_IT[branchN-1] + noiseIT)
                    measureITMag += [[fbus,tbus, noisySignalIT]]

        return  [measureIFMag,measureITMag]

    def generateMeasurementsSCADA_S(self):
        ideal_SF = self.idealValues["ideal_SBranchF"]
        ideal_ST = self.idealValues["ideal_SBranchT"]

        measureSFMag   = [] 
        measureSTMag   = []

        for bus in self.sensors.arraySCADA:
            busNumber           = bus[0]
            TVE                 = bus[1].TVE
            for branch  in self.topology.branches:
                branchN     = branch[self.topology.ref.BR_i].astype(int)
                fbus        = branch[self.topology.ref.BR_fbus]
                tbus        = branch[self.topology.ref.BR_tbus]

                # Generación de mediciones Ifrom (El sensor está situado en un  nodo from)

                if busNumber == fbus:
                    noiseSF = self.circularNoiseGenerator(self.nSamples,ideal_SF[branchN-1],TVE=TVE)
                    noisySignalSF = ideal_SF[branchN-1] + noiseSF
                    measureSFMag += [[fbus,tbus, noisySignalSF]]
                    
                #  Generación de mediciones Ito (El sensor está situado en un nodo to)
                if busNumber == tbus:
                    noiseST = self.circularNoiseGenerator(self.nSamples,ideal_ST[branchN-1],TVE=TVE)
                    noisySignalST = ideal_ST[branchN-1] + noiseST
                    measureSTMag += [[fbus,tbus, noisySignalST]]

        return  [measureSFMag,measureSTMag]

    def plotBusValuesInterval(self,variable="V",interval=0,buses=[]):

        if variable == "V":
            signalToPlot = self.idealValues["ideal_V"]
        elif variable  == "I":
            signalToPlot = self.idealValues["ideal_IBus"]
        elif variable  == "S":
            signalToPlot = self.idealValues["ideal_SBus"]
        else:
            raise Exception("Variable  must be <V|I|S>")

        if interval == 0:
            interval = range(self.nSamples)

        if  buses  == []:
            buses = np.array([x for x in range(1,self.topology.nBuses + 1)])

        plt.rcParams.update({'font.size': 5})
        axisSubplots = np.ceil(np.sqrt(len(buses))).astype(int) 
        subplotIndex = 1
        plt.figure(0)
        plt.suptitle('Módulo', fontsize=12)
        plt.tight_layout()
        plt.figure(1)
        plt.suptitle('Fase', fontsize=12)
        plt.tight_layout()

        for bus in buses:
            plt.figure(0)
            plt.subplot(axisSubplots,axisSubplots,subplotIndex)
            plt.plot(np.abs(signalToPlot[bus-1,interval]))
            plt.grid()
            plt.gca().set_title("Nodo: " + str(bus))

            plt.figure(1)
            plt.subplot(axisSubplots,axisSubplots,subplotIndex)
            plt.plot(np.angle(signalToPlot[bus-1,interval]))
            plt.grid()
            plt.gca().set_title("Nodo: " + str(bus))
            
            subplotIndex += 1

        plt.show()
       
    def outputArrayGenerator(self):
        """Por ahora levanto la señal trueSignal pero luego deberá levantar la señal de mediciones generada a partir de trueSignal y Topology  (que  aportará la información de ruido de los
        sensores)"""
        return self.trueSignal

    def idealValuesValidator(self):
        VBus = self.idealValues['ideal_V']
        IBus = self.idealValues['ideal_IBus']
        SBus = self.idealValues['ideal_SBus']
        ISh  = self.idealValues['ideal_IShunt']
        IBranchF = self.idealValues['ideal_IBranchF']
        IBranchT = self.idealValues['ideal_IBranchT']
        SBranchF = self.idealValues['ideal_SBranchF']
        SBranchT = self.idealValues['ideal_SBranchT']

        instante = 0
        nodo     = 4

        listFromNodeBranches    =  self.topology.fromNodeBranches(nodo)
        listToNodeBranches      =  self.topology.toNodeBranches(nodo)

        print("ListFromNodeBranches: " + np.array_str(np.array(listFromNodeBranches)))
        print("ListToNodeBranches: " + np.array_str(np.array(listToNodeBranches)))

        corrientesFrom  = [IBranchF[(x-1).astype(int),instante] for x in listFromNodeBranches]
        corrientesTo    = [IBranchT[(x-1).astype(int),instante] for x in listToNodeBranches]
        corrientesSh    = ISh[nodo-1,instante]
        print("Corriente SH", corrientesSh)
        sumaDeCorrientes = np.sum(np.append(np.append(corrientesFrom,corrientesTo),corrientesSh))

        potenciasFrom  = [SBranchF[(x-1).astype(int),instante] for x in listFromNodeBranches]
        potenciasTo    = [SBranchT[(x-1).astype(int),instante] for x in listToNodeBranches]
        sumaDePotencias = np.sum(np.append(potenciasFrom,potenciasTo))

        if np.abs(sumaDeCorrientes) < 1E-13:
            sumaDeCorrientes = 0

        if np.abs(sumaDePotencias) < 1E-13:
            sumaDePotencias = 0

        print("Suma de corrientes en el nodo: ", sumaDeCorrientes)
        print("Suma de potencias en el nodo: ", sumaDePotencias)

        instantePrueba = 40
        nodoPrueba = 4
        pruebaV = self.idealValues['ideal_V'][:,instantePrueba]
        pruebaI = self.idealValues['ideal_IBus'][:,instantePrueba]
        pruebaS = self.idealValues['ideal_SBus'][:,instantePrueba]
        pruebaIS = self.idealValues['ideal_IShunt'][:,instantePrueba]
        pruebaIF = self.idealValues['ideal_IBranchF'][:,instantePrueba]
        pruebaIT = self.idealValues['ideal_IBranchT'][:,instantePrueba]
        pruebaSF = self.idealValues['ideal_SBranchF'][:,instantePrueba]
        pruebaST = self.idealValues['ideal_SBranchT'][:,instantePrueba]
        pruebaSS = self.idealValues['ideal_SShunt'][:,instantePrueba]


        auxIBus  = np.zeros((np.shape(pruebaI)),dtype=np.complex)
        for branch in self.topology.branches:
            branchN = branch[self.topology.ref.BR_i].astype(int)
            fbus  = branch[self.topology.ref.BR_fbus].astype(int)
            tbus  = branch[self.topology.ref.BR_tbus].astype(int)

            auxIBus[fbus-1] = auxIBus[fbus-1] + pruebaIF[branchN-1]
            auxIBus[tbus-1] = auxIBus[tbus-1] + pruebaIT[branchN-1]
            
        print("IBus: ", pruebaI[nodoPrueba-1])
        print("IShunt: ", pruebaIS[nodoPrueba-1])
        print("auxIBus: ", auxIBus[nodoPrueba-1])
        print("auxIBus + IShunt: ", pruebaIS[nodoPrueba-1] + auxIBus[nodoPrueba-1])      

        instantePrueba = 40
        nodoPrueba = 10
        pruebaV = self.idealValues['ideal_V'][:,instantePrueba]
        pruebaI = self.idealValues['ideal_IBus'][:,instantePrueba]
        pruebaS = self.idealValues['ideal_SBus'][:,instantePrueba]
        pruebaIS = self.idealValues['ideal_IShunt'][:,instantePrueba]
        pruebaIF = self.idealValues['ideal_IBranchF'][:,instantePrueba]
        pruebaIT = self.idealValues['ideal_IBranchT'][:,instantePrueba]
        pruebaSF = self.idealValues['ideal_SBranchF'][:,instantePrueba]
        pruebaST = self.idealValues['ideal_SBranchT'][:,instantePrueba]
        pruebaSS = self.idealValues['ideal_SShunt'][:,instantePrueba]


        auxIBus  = np.zeros((np.shape(pruebaI)),dtype=np.complex)
        for branch in self.topology.branches:
            branchN = branch[self.topology.ref.BR_i].astype(int)
            fbus  = branch[self.topology.ref.BR_fbus].astype(int)
            tbus  = branch[self.topology.ref.BR_tbus].astype(int)

            auxIBus[fbus-1] = auxIBus[fbus-1] + pruebaIF[branchN-1]
            auxIBus[tbus-1] = auxIBus[tbus-1] + pruebaIT[branchN-1]
            
        print("IBus: ", pruebaI[nodoPrueba-1])
        print("IShunt: ", pruebaIS[nodoPrueba-1])
        print("auxIBus: ", auxIBus[nodoPrueba-1])
        print("auxIBus + IShunt: ", pruebaIS[nodoPrueba-1] + auxIBus[nodoPrueba-1])



        auxSBus  = np.zeros((np.shape(pruebaS)),dtype=np.complex)
        for branch in self.topology.branches:
            branchN = branch[self.topology.ref.BR_i].astype(int)
            fbus  = branch[self.topology.ref.BR_fbus].astype(int)
            tbus  = branch[self.topology.ref.BR_tbus].astype(int)

            auxSBus[fbus-1] = auxSBus[fbus-1] + pruebaSF[branchN-1]
            auxSBus[tbus-1] = auxSBus[tbus-1] + pruebaST[branchN-1]
            
        print("SBus: ", pruebaS[nodoPrueba-1])
        print("SShunt: ", pruebaSS[nodoPrueba-1])
        print("auxSBus: ", auxSBus[nodoPrueba-1])
        print("auxSBus + SShunt: ", pruebaSS[nodoPrueba-1] + auxSBus[nodoPrueba-1])

    def circularNoiseGenerator(self,nSamples,ideal,TVE=1,cartesian=True):

        factorTVE           = TVE/100
        factorNoisePMU      = np.abs(ideal)*factorTVE   
        signalNoiseRadious  = factorNoisePMU * np.sqrt(np.random.uniform(size=nSamples))
        signalNoiseTheta    = np.random.uniform(size=nSamples) * 2 * np.pi
        signalNoiseReal     = signalNoiseRadious*np.cos(signalNoiseTheta)
        signalNoiseImag     = signalNoiseRadious*np.sin(signalNoiseTheta)
        signalNoiseComplex  = signalNoiseReal + 1j*signalNoiseImag

        if cartesian:
            return signalNoiseComplex
        return [signalNoiseRadious,signalNoiseTheta]

    def signalCompare(self,signal1,signal2):
        if len(signal1)  != len(signal2):
            raise Exception("Cant compare different size signals")
        
        plt.figure()
        plt.subplot(121)
        plt.plot(np.abs(signal1))
        plt.plot(np.abs(signal2),".")
        plt.grid()
        plt.subplot(122)
        plt.plot(np.angle(signal1))
        plt.plot(np.angle(signal2),".")
        plt.grid()
        plt.show()
        sys.exit()

    def tveCalculator(self,ideal,measurement):
        print(measurement)
        if np.sum(np.abs(np.imag(measurement))) == 0:
            print("holas")
            ideal =np.abs(ideal)
        TVE = np.abs(ideal-measurement)*100/np.abs(ideal)
        plt.figure()
        plt.plot(TVE,'.')

class SensorNetwork:
    def __init__(self,topology="",sensorNetworkData={}):

        defaultSCADATVE         = 5   
        defaultSCADAReportTime  = 2000  # milliseconds

        if topology == "":
            raise Exception("You must define a topology instance")   
        self.topology = topology
        if sensorNetworkData == {}:
            raise Exception("You must define a sensor network")
       
        busesPMU    = sensorNetworkData['busesPMU']     # Location, report time and TVE for each PMU sensor on the network
        busesSCADA  = sensorNetworkData['busesSCADA']   # Location, report time and TVE for each SCADA sensor on the network

        if busesSCADA == []:
            busesSCADA = [(x,defaultSCADAReportTime,defaultSCADATVE) for x in range(1,topology.nBuses)]

        self.arrayPMU = []
        for bus in busesPMU:
            sensorPMU = SensorPMU(bus[0],TVE=bus[1]) # (reportTime, TVE)
            self.arrayPMU  = self.arrayPMU + [[bus[0],sensorPMU]]

        self.arraySCADA = []
        for bus in busesSCADA:
            sensorSCADA = SensorSCADA(bus[0],TVE=bus[1]) # (reportTime, TVE)
            self.arraySCADA  = self.arraySCADA + [[bus[0],sensorSCADA]]

        self.locationPMU = busesPMU
        self.locationSCADA = busesSCADA
        self.SCADAReportTime = self.arraySCADA[0][1].reportTime # For the moment the reportTime  for SCADA and  PMU is Global
        self.PMUReportTime = self.arrayPMU[0][1].reportTime # For the moment the reportTime  for SCADA and  PMU is Global
        self.observabilityPMU()


    def observabilityPMU(self):
        #   BusN  NeighborsList    BusN   NeighborsList
        # [[  1,    [5]   ],   [   5,    []  1,  10 ]]]

        self.arrayObsPMU = []
        for busPMU  in self.locationPMU:
            busNumber       = busPMU[0]
            neighborSetAux  = set() 
            for branch in self.topology.branches:
                edges = set([branch[self.topology.ref.BR_fbus].astype(int),branch[self.topology.ref.BR_tbus].astype(int)])
                if busNumber in edges:
                    neighborSetAux.add( (edges - {busNumber}).pop() )
            neighborListAux = list(neighborSetAux)
            neighborListAux.sort()
            self.arrayObsPMU = self.arrayObsPMU + [[busNumber, neighborListAux]]

    def printData(self):

        print("-"*15," PMU Network ","-"*15)
        for x  in self.arrayPMU:
            print(x[1].info)
        print("-"*15,"SCADA Network","-"*15)
        for x  in self.arraySCADA:
            print(x[1].info)
        print("-" * 45)

class SensorPMU:
    def __init__(self,bus="",reportTime=16,TVE=1):
        if bus == "":
            raise Exception("You must define a Bus for instantiating a PMU sensor")
        
        self.bus        = bus
        self.reportTime = reportTime
        self.TVE        = TVE
        self.info = {"Bus" : bus, "Report Time": reportTime, "TVE" : TVE} 

class SensorSCADA:
    def __init__(self,bus="",reportTime=2000,TVE=5):
        if bus == "":
            raise Exception("You must define a Bus for instantiating a PMU sensor")

        self.bus        = bus
        self.reportTime = reportTime
        self.TVE        = TVE
        self.info = {"Bus" : bus, "Report Time": reportTime, "TVE" : TVE} 
        



""" RESUMEN DE ESTA CLASE
Esta clase está dedicada a simular la obtención, envío y recolección de medición SCADA y PMU.
En linea con esto se definieron los siguientes métodos:

* measurementSocketServer: Monta un servidor encargado de disponibilizar las muestras mediante socket.io. En una primera instancia
estas muestras se  enviarán en dos tipos de bloque, PMUOnly y PMU+SCADA. Una posible mejora sería independizar el tiempo de envío 
de cada sensor para que se asemeje más a la realidad. Este método requiere conocer la distribución de los sensores de la red para 
determinar que mediciones debe incorporar al vector enviado.

* measurementSocketClient: Simula  el módulo del centro de control encargado de recolectar las  mediciones de los sensores.

* trueVSignalLoader: Para simular mediciones se requiere una señal ideal conocida. Cuando la señal ideal sea del fasor de tensión este
método se encargará de cargarla y formatearla. 

* measurementGenerator: En línea con el método trueVSignalLoader, este método se encarga de generar mediciones de todas las variables
eléctricas presentes en la topología. Este método requiere conocer la topología de red. Referirse a la clase Topology
para más detalles.
"""