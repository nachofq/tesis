from classes.topology import Topology
from classes.measurements import Measurements
from classes.measurements import SensorNetwork
from classes.socketMsg import SocketMsg
import classes.topology 
from classes.customCases import case13
import numpy as np
import pypower.api as pypower
import sys

# Topology definition
topology                        = Topology(case13())

# Sensor Network definition
sensorNetworkData               = {}
sensorNetworkData["busesPMU"]   = [(1,1), (2,1),(5,1),(11,1),(13,1)]
sensorNetworkData["busesSCADA"] = []
sensorNetwork                   = SensorNetwork(topology,sensorNetworkData)

# Measurements  generation
m = Measurements(trueSignalPath="../data/phasors_3phasefault.mat",idealSignalSampleRate=16,topology=topology,sensors=sensorNetwork)
dataArrayTest = m.outputArrayGenerator()

# Stream measurements
socketSrv  = SocketMsg("server", dataArrayTest)







#m.plotBusValuesInterval(variable="S")
#t.printNetwork()
#dataArrayTest = m.outputArrayGenerator()


